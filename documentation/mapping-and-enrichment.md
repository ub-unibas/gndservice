# Mapping

## General for 1xx/7xx

A JSON object is created for each MARC field and any linked 880 fields. This object contains an object for each authority file, which in turn contains the metadata describing the entity. [MODS](https://www.loc.gov/standards/mods/) has been used as the basis for the mapping of the MARC subfields but has been extended where necessary.

Example of a person, described in field 100 and a linked field 880:
* [Output from the person API for Una Jacobs](https://gitlab.switch.ch/ub-unibas/gndservice/-/blob/main/documentation/examples/person.json)
* [MARC record](https://gitlab.switch.ch/ub-unibas/gndservice/-/blob/main/documentation/examples/marc-with-person.xml) which was used as input


## General for access points for titles

The gndservice considers the following fields to be access points for titles. These fields are processed by the title and subject.title APIs and a title object is created. These fields are not processed by the person, corporate, conference and family APIs.

* Fields x30
* Fields x00, x10, x11 containing subfield $t

## Mapping and object structures

To further understand the structure of the objects and the mapping of the data, see the following parts of the code:
* Person: [person.go](https://gitlab.switch.ch/ub-unibas/gndservice/-/blob/main/pkg/gndstructservice/person.go)
* Corporate: [corporate.go](https://gitlab.switch.ch/ub-unibas/gndservice/-/blob/main/pkg/gndstructservice/corporate.go)
* Conference: [conference.go](https://gitlab.switch.ch/ub-unibas/gndservice/-/blob/main/pkg/gndstructservice/conference.go)
* Family: [family.go](https://gitlab.switch.ch/ub-unibas/gndservice/-/blob/main/pkg/gndstructservice/family.go)
* Geographic: [geo.go](https://gitlab.switch.ch/ub-unibas/gndservice/-/blob/main/pkg/gndstructservice/geo.go)
* Title: [title.go](https://gitlab.switch.ch/ub-unibas/gndservice/-/blob/main/pkg/gndstructservice/title.go)
* Topic: [topic.go](https://gitlab.switch.ch/ub-unibas/gndservice/-/blob/main/pkg/gndstructservice/topic.go)


# Enrichment

For all entities which are linked by identifier, subfield $0 starting with (DE-588), the following fields are enriched from GND:
* corresponding fields 4xx in field variant
* 024 $a, prefixed with content from $2 in brackets in field otherIdentifier
* 042 $a in field level
* 075 $b in field type
* 079 $q in field useFor

Additional enrichments are performed for some entities.

## General for access points for subjects 6xx
For some entities the names of related entities (5xx) are enriched in field related:
* Person: if subfield $4 contains nawi (wirklicher Name) or pseu (Pseudonym)
* Corporate: if subfield $4 contains vorg (Vorgänger), nach (Nachfolger), nazw (zeitweiser Name)
* Geographic: if subfield $4 contains vorg (Vorgänger), nach (Nachfolger), nazw (zeitweiser Name)

## Person
* 375 $a in field gender, mapped to a [label](https://gitlab.switch.ch/ub-unibas/gndservice/-/blob/main/pkg/gndstructservice/person.go#L13)
* 550 $a if subfield $4 contains berc or beru in field profession
* 551 $a if subfield $4 contains ortg in field placeOfBirth
* 551 $a if subfield $4 contains ortw in field placeOfActivity

## Corporate
* 451 $a in field variant
* 551 $a if subfield $4 contains orta in field placeOfBusiness

## Geographic
* 034 $f, $g for geonames in field coordinates
* 034 $0 for geonames in field geoNamesID

## Topic
* 750 $a in field mapped
* If a field 260 is present in the GND record, the string "Hinweissatz" is added to the field useFor

## Mapping GND
To further understand the mapping of the data, see the following parts of the code:
* Person: [gnd.go](https://gitlab.switch.ch/ub-unibas/gndservice/-/blob/main/pkg/gndstructservice/gnd.go)
* Corporate: [corporateGND.go](https://gitlab.switch.ch/ub-unibas/gndservice/-/blob/main/pkg/gndstructservice/corporateGND.go)
* Conference: [conferenceGND.go](https://gitlab.switch.ch/ub-unibas/gndservice/-/blob/main/pkg/gndstructservice/conferenceGND.go)
* Family: [familyGND.go](https://gitlab.switch.ch/ub-unibas/gndservice/-/blob/main/pkg/gndstructservice/familyGND.go)
* Geographic: [geoGND.go](https://gitlab.switch.ch/ub-unibas/gndservice/-/blob/main/pkg/gndstructservice/geoGND.go)
* Title: [titleGND.go](https://gitlab.switch.ch/ub-unibas/gndservice/-/blob/main/pkg/gndstructservice/titleGND.go)
* Topic: [topicGND.go](https://gitlab.switch.ch/ub-unibas/gndservice/-/blob/main/pkg/gndstructservice/topicGND.go)