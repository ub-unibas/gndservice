package shared

import (
	"crypto/tls"
	"crypto/x509"
	"emperror.dev/errors"
	ublogger "gitlab.switch.ch/ub-unibas/go-ublogger/v2"
	"os"
)

const ConfigFileLocationEnvVariable = "CONFIG_FILE_LOCATION"

func LoadCertificateConfig(enabled bool, tlsCertPath, tlsKeyPath, tlsCaCert string, logger *ublogger.Logger) (*tls.Config, error) {
	if !enabled {
		return nil, nil
	}
	if tlsCertPath == "" || tlsKeyPath == "" {
		return nil, errors.New("tls certificate or key path not set")
	}
	certificate, err := tls.LoadX509KeyPair(tlsCertPath, tlsKeyPath)
	if err != nil {
		logger.Fatal().Msgf("cannot load tls certificate: %v", err)
		return nil, err
	}

	caCert, err := os.ReadFile(tlsCaCert)
	if err != nil {
		return nil, errors.New("CA certificate could not be read: " + err.Error())
	}
	caCertPool, err := x509.SystemCertPool()
	if err != nil {
		return nil, errors.New("Could not load system cert pool: " + err.Error())
	}
	caCertPool.AppendCertsFromPEM(caCert)

	return &tls.Config{
		Certificates: []tls.Certificate{certificate},
		RootCAs:      caCertPool,
	}, nil
}
