//go:generate swag init --parseDependency  --parseInternal -g api.go

package gndstructservice

import (
	"context"
	"crypto/tls"
	"emperror.dev/errors"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/google/martian/log"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"gitlab.switch.ch/ub-unibas/gndservice/v2/pkg/gndstructservice/docs"
	"gitlab.switch.ch/ub-unibas/gndservice/v2/pkg/marc21struct"
	"golang.org/x/net/http2"
	"net/http"
	"net/url"
	"strings"
	"sync"
)

const ApiBasePath = "/api/v1"

type marc21Service interface {
	Do(*marc21struct.QueryStruct) ([]any, error)
}

type SrvParam struct {
	Marc    *marc21struct.QueryStruct  `json:"marc"`
	Mapping map[string]json.RawMessage `json:"mapping,omitempty"`
}

//	@title			GND Structure Enrichment API
//	@version		1.0
//	@description	This API is a wrapper around the GND Service that enriches MARC21 data with GND data. The MARC21 data has to be supplied in a specific structure for it to work with the relevant fields. For each GND linked entity one enriched entity is returned.
//	@termsOfService	http://swagger.io/terms/

//	@contact.name	University Library Basel, Informatik
//	@contact.url	https://ub.unibas.ch
//	@contact.email	it-ub@unibas.ch

//	@license.name	Apache 2.0
//	@license.url	http://www.apache.org/licenses/LICENSE-2.0.html

// @securityDefinitions.apikey	ApiKeyAuth
// @in							header
// @name						Authorization
// @description					Bearer Authentication with JWT

func NewController(
	domain string,
	port int,
	proxyUri string,
	tlsConfig *tls.Config,
	gndServiceUrl *url.URL,
	client *http.Client,
) (*Controller, error) {
	externalUrl, err := url.Parse(proxyUri)
	if err != nil {
		return nil, errors.Wrapf(err, "invalid external address '%s'", proxyUri)
	}
	apiPath := "/" + strings.Trim(externalUrl.Path, "/")

	// programmatically set swagger info
	if externalUrl.Port() == "" {
		docs.SwaggerInfo.Host = externalUrl.Hostname()
	} else {
		docs.SwaggerInfo.Host = fmt.Sprintf("%s:%s", externalUrl.Hostname(), externalUrl.Port())
	}
	docs.SwaggerInfo.BasePath = "/" + strings.Trim(apiPath+ApiBasePath, "/")
	docs.SwaggerInfo.Schemes = []string{externalUrl.Scheme}

	c := &Controller{
		gndServiceUrl: gndServiceUrl,
		service:       make(map[string]marc21Service),
		gnd:           NewGNDService(client, gndServiceUrl),
	}
	if err := c.Init(domain, port, tlsConfig); err != nil {
		return nil, errors.Wrap(err, "cannot initialize rest controller")
	}
	return c, nil
}

type Controller struct {
	server        http.Server
	gndServiceUrl *url.URL
	service       map[string]marc21Service
	gnd           *gnd
}

func (ctrl *Controller) Init(domain string, port int, tlsConfig *tls.Config) error {
	router := gin.Default()
	v1 := router.Group(ApiBasePath)
	v1.POST("/family", ctrl.getFamily)
	v1.POST("/subject.family", ctrl.getFamilySubject)

	v1.POST("/conference", ctrl.getConference)
	v1.POST("/subject.conference", ctrl.getConferenceSubject)

	v1.POST("/corporate", ctrl.getCorporate)
	v1.POST("/subject.corporate", ctrl.getCorporateSubject)

	v1.POST("/geo", ctrl.getGeo)
	v1.POST("/subject.geo", ctrl.getGeoSubject)

	v1.POST("/title", ctrl.getTitle)
	v1.POST("/subject.title", ctrl.getTitleSubject)

	v1.POST("/person", ctrl.getPerson)
	v1.POST("/subject.person", ctrl.getPersonSubject)

	v1.POST("/topic", ctrl.getTopic)

	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	ctrl.server = http.Server{
		Addr:      fmt.Sprintf("%s:%d", domain, port),
		Handler:   router,
		TLSConfig: tlsConfig,
	}
	if err := http2.ConfigureServer(&ctrl.server, nil); err != nil {
		return errors.WithStack(err)
	}

	return nil
}

func (ctrl *Controller) Start(wg *sync.WaitGroup, tlsEnabled bool) {
	go func() {
		wg.Add(1)
		defer wg.Done() // let main know we are done cleaning up
		if tlsEnabled {
			if err := ctrl.server.ListenAndServeTLS("", ""); err != nil {
				log.Errorf("https server (%s) could not be started: %v", ctrl.server.Addr, err)
			}
		} else {
			if err := ctrl.server.ListenAndServe(); err != nil {
				log.Errorf("http server (%s) could not be started: %v", ctrl.server.Addr, err)
			}
		}
	}()
}

func (ctrl *Controller) Stop() {
	err := ctrl.server.Shutdown(context.Background())
	if err != nil {
		return
	}
}

func (ctrl *Controller) GracefulStop() {
	err := ctrl.server.Shutdown(context.Background())
	if err != nil {
		return
	}
}

// newMediaItem godoc
// @Summary      person enrichment
// @Description  Takes the relevant MARC data of an entry and returns an enriched person structure
// @Security 	 ApiKeyAuth
// @Tags         GND
// @Produce      json
// @Param	     MarcQueryStruct	body		marc21struct.QueryStruct	true	"Get "
// @Success      200  {object}  []any
// @Failure      400  {object}  gndstructservice.HTTPResultMessage
// @Failure      404  {object}  gndstructservice.HTTPResultMessage
// @Failure      500  {object}  gndstructservice.HTTPResultMessage
// @Router       /person [POST]
func (ctrl *Controller) getPerson(c *gin.Context) {
	var requestBody = &marc21struct.QueryStruct{}
	if err := c.BindJSON(requestBody); err != nil {
		NewResultMessage(c, http.StatusBadRequest, err)
		return
	}
	result, err := person(requestBody, ctrl.gnd)
	if err != nil {
		NewResultMessage(c, http.StatusInternalServerError, err)
		return
	}
	if result == nil {
		result = []any{}
	}
	if c.Query("format") == "xml" {
		c.XML(http.StatusOK, result)
		return
	}
	c.JSON(http.StatusOK, result)
}

// newMediaItem godoc
// @Summary      get personSubject
// @Description  Takes the relevant MARC data of an entry and returns an enriched person structure
// @Security 	 ApiKeyAuth
// @Tags         GND
// @Produce      json
// @Param	     MarcQueryStruct	body		marc21struct.QueryStruct	true	"Get "
// @Success      200  {object}  []any
// @Failure      400  {object}  gndstructservice.HTTPResultMessage
// @Failure      404  {object}  gndstructservice.HTTPResultMessage
// @Failure      500  {object}  gndstructservice.HTTPResultMessage
// @Router       /subject.person [POST]
func (ctrl *Controller) getPersonSubject(c *gin.Context) {
	var requestBody = &marc21struct.QueryStruct{}
	if err := c.BindJSON(requestBody); err != nil {
		NewResultMessage(c, http.StatusBadRequest, err)
		return
	}
	result, err := subjectPerson(requestBody, ctrl.gnd)
	if err != nil {
		NewResultMessage(c, http.StatusInternalServerError, err)
		return
	}
	if result == nil {
		result = []any{}
	}
	if c.Query("format") == "xml" {
		c.XML(http.StatusOK, result)
		return
	}
	c.JSON(http.StatusOK, result)
}

// newMediaItem godoc
// @Summary      family enrichment
// @Description  Takes the relevant MARC data of an entry and returns an enriched family structure
// @Security 	 ApiKeyAuth
// @Tags         GND
// @Produce      json
// @Param	     MarcQueryStruct	body		marc21struct.QueryStruct	true	"Get "
// @Success      200  {object}  []any
// @Failure      400  {object}  gndstructservice.HTTPResultMessage
// @Failure      404  {object}  gndstructservice.HTTPResultMessage
// @Failure      500  {object}  gndstructservice.HTTPResultMessage
// @Router       /family [POST]
func (ctrl *Controller) getFamily(c *gin.Context) {
	var requestBody = &marc21struct.QueryStruct{}
	if err := c.BindJSON(requestBody); err != nil {
		NewResultMessage(c, http.StatusBadRequest, err)
		return
	}
	result, err := family(requestBody, ctrl.gnd)
	if err != nil {
		NewResultMessage(c, http.StatusInternalServerError, err)
		return
	}
	if result == nil {
		result = []any{}
	}
	if c.Query("format") == "xml" {
		c.XML(http.StatusOK, result)
		return
	}
	c.JSON(http.StatusOK, result)
}

// newMediaItem godoc
// @Summary      family subject enrichment
// @Description  Takes the relevant MARC data of an entry and returns an enriched family structure
// @Security 	 ApiKeyAuth
// @Tags         GND
// @Produce      json
// @Param	     MarcQueryStruct	body		marc21struct.QueryStruct	true	"Get "
// @Success      200  {object}  []any
// @Failure      400  {object}  gndstructservice.HTTPResultMessage
// @Failure      404  {object}  gndstructservice.HTTPResultMessage
// @Failure      500  {object}  gndstructservice.HTTPResultMessage
// @Router       /subject.family [POST]
func (ctrl *Controller) getFamilySubject(c *gin.Context) {
	var requestBody = &marc21struct.QueryStruct{}
	if err := c.BindJSON(requestBody); err != nil {
		NewResultMessage(c, http.StatusBadRequest, err)
		return
	}
	result, err := subjectFamily(requestBody, ctrl.gnd)
	if err != nil {
		NewResultMessage(c, http.StatusInternalServerError, err)
		return
	}
	if result == nil {
		result = []any{}
	}
	if c.Query("format") == "xml" {
		c.XML(http.StatusOK, result)
		return
	}
	c.JSON(http.StatusOK, result)
}

// newMediaItem godoc
// @Summary      geo enrichment
// @Description  Takes the relevant MARC data of an entry and returns an enriched geo structure
// @Security 	 ApiKeyAuth
// @Tags         GND
// @Produce      json
// @Param	     MarcQueryStruct	body		marc21struct.QueryStruct	true	"Get "
// @Success      200  {object}  []any
// @Failure      400  {object}  gndstructservice.HTTPResultMessage
// @Failure      404  {object}  gndstructservice.HTTPResultMessage
// @Failure      500  {object}  gndstructservice.HTTPResultMessage
// @Router       /geo [POST]
func (ctrl *Controller) getGeo(c *gin.Context) {
	var requestBody = &marc21struct.QueryStruct{}
	if err := c.BindJSON(requestBody); err != nil {
		NewResultMessage(c, http.StatusBadRequest, err)
		return
	}
	result, err := geo(requestBody, ctrl.gnd)
	if err != nil {
		NewResultMessage(c, http.StatusInternalServerError, err)
		return
	}
	if result == nil {
		result = []any{}
	}
	if c.Query("format") == "xml" {
		c.XML(http.StatusOK, result)
		return
	}
	c.JSON(http.StatusOK, result)
}

// newMediaItem godoc
// @Summary      geo from subject enrichment
// @Description  Takes the relevant MARC data of an entry and returns an enriched geo structure
// @Security 	 ApiKeyAuth
// @Tags         GND
// @Produce      json
// @Param	     MarcQueryStruct	body		marc21struct.QueryStruct	true	"Get "
// @Success      200  {object}  []any
// @Failure      400  {object}  gndstructservice.HTTPResultMessage
// @Failure      404  {object}  gndstructservice.HTTPResultMessage
// @Failure      500  {object}  gndstructservice.HTTPResultMessage
// @Router       /subject.geo [POST]
func (ctrl *Controller) getGeoSubject(c *gin.Context) {
	var requestBody = &marc21struct.QueryStruct{}
	if err := c.BindJSON(requestBody); err != nil {
		NewResultMessage(c, http.StatusBadRequest, err)
		return
	}
	result, err := subjectGeo(requestBody, ctrl.gnd)
	if err != nil {
		NewResultMessage(c, http.StatusInternalServerError, err)
		return
	}
	if result == nil {
		result = []any{}
	}
	if c.Query("format") == "xml" {
		c.XML(http.StatusOK, result)
		return
	}
	c.JSON(http.StatusOK, result)
}

// newMediaItem godoc
// @Summary      conference enrichment
// @Description  Takes the relevant MARC data of an entry and returns an enriched conference structure
// @Security 	 ApiKeyAuth
// @Tags         GND
// @Produce      json
// @Param	     MarcQueryStruct	body		marc21struct.QueryStruct	true	"Get "
// @Success      200  {object}  []any
// @Failure      400  {object}  gndstructservice.HTTPResultMessage
// @Failure      404  {object}  gndstructservice.HTTPResultMessage
// @Failure      500  {object}  gndstructservice.HTTPResultMessage
// @Router       /conference [POST]
func (ctrl *Controller) getConference(c *gin.Context) {
	var requestBody = &marc21struct.QueryStruct{}
	if err := c.BindJSON(requestBody); err != nil {
		NewResultMessage(c, http.StatusBadRequest, err)
		return
	}
	result, err := conference(requestBody, ctrl.gnd)
	if err != nil {
		NewResultMessage(c, http.StatusInternalServerError, err)
		return
	}
	if result == nil {
		result = []any{}
	}
	if c.Query("format") == "xml" {
		c.XML(http.StatusOK, result)
		return
	}
	c.JSON(http.StatusOK, result)
}

// newMediaItem godoc
// @Summary      conference from subject field enrichment
// @Description  Takes the relevant MARC data of an entry and returns an enriched conference structure
// @Security 	 ApiKeyAuth
// @Tags         GND
// @Produce      json
// @Param	     MarcQueryStruct	body		marc21struct.QueryStruct	true	"Get "
// @Success      200  {object}  []any
// @Failure      400  {object}  gndstructservice.HTTPResultMessage
// @Failure      404  {object}  gndstructservice.HTTPResultMessage
// @Failure      500  {object}  gndstructservice.HTTPResultMessage
// @Router       /subject.conference [POST]
func (ctrl *Controller) getConferenceSubject(c *gin.Context) {
	var requestBody = &marc21struct.QueryStruct{}
	if err := c.BindJSON(requestBody); err != nil {
		NewResultMessage(c, http.StatusBadRequest, err)
		return
	}
	result, err := subjectConference(requestBody, ctrl.gnd)
	if err != nil {
		NewResultMessage(c, http.StatusInternalServerError, err)
		return
	}
	if result == nil {
		result = []any{}
	}
	if c.Query("format") == "xml" {
		c.XML(http.StatusOK, result)
		return
	}
	c.JSON(http.StatusOK, result)
}

// newMediaItem godoc
// @Summary      corporate enrichment
// @Description  Takes the relevant MARC data of an entry and returns an enriched corporate structure
// @Security 	 ApiKeyAuth
// @Tags         GND
// @Produce      json
// @Param	     MarcQueryStruct	body		marc21struct.QueryStruct	true	"Get "
// @Success      200  {object}  []any
// @Failure      400  {object}  gndstructservice.HTTPResultMessage
// @Failure      404  {object}  gndstructservice.HTTPResultMessage
// @Failure      500  {object}  gndstructservice.HTTPResultMessage
// @Router       /corporate [POST]
func (ctrl *Controller) getCorporate(c *gin.Context) {
	var requestBody = &marc21struct.QueryStruct{}
	if err := c.BindJSON(requestBody); err != nil {
		NewResultMessage(c, http.StatusBadRequest, err)
		return
	}
	result, err := corporate(requestBody, ctrl.gnd)
	if err != nil {
		NewResultMessage(c, http.StatusInternalServerError, err)
		return
	}
	if result == nil {
		result = []any{}
	}
	if c.Query("format") == "xml" {
		c.XML(http.StatusOK, result)
		return
	}
	c.JSON(http.StatusOK, result)
}

// newMediaItem godoc
// @Summary      corporate from subject enrichment
// @Description  Takes the relevant MARC data of an entry and returns an enriched corporate structure
// @Security 	 ApiKeyAuth
// @Tags         GND
// @Produce      json
// @Param	     MarcQueryStruct	body		marc21struct.QueryStruct	true	"Get "
// @Success      200  {object}  []any
// @Failure      400  {object}  gndstructservice.HTTPResultMessage
// @Failure      404  {object}  gndstructservice.HTTPResultMessage
// @Failure      500  {object}  gndstructservice.HTTPResultMessage
// @Router       /subject.corporate [POST]
func (ctrl *Controller) getCorporateSubject(c *gin.Context) {
	var requestBody = &marc21struct.QueryStruct{}
	if err := c.BindJSON(requestBody); err != nil {
		NewResultMessage(c, http.StatusBadRequest, err)
		return
	}
	result, err := subjectCorporate(requestBody, ctrl.gnd)
	if err != nil {
		NewResultMessage(c, http.StatusInternalServerError, err)
		return
	}
	if result == nil {
		result = []any{}
	}
	if c.Query("format") == "xml" {
		c.XML(http.StatusOK, result)
		return
	}
	c.JSON(http.StatusOK, result)
}

// newMediaItem godoc
// @Summary      title enrichment
// @Description  Takes the relevant MARC data of an entry and returns an enriched title structure
// @Security 	 ApiKeyAuth
// @Tags         GND
// @Produce      json
// @Param	     MarcQueryStruct	body		marc21struct.QueryStruct	true	"Get "
// @Success      200  {object}  []any
// @Failure      400  {object}  gndstructservice.HTTPResultMessage
// @Failure      404  {object}  gndstructservice.HTTPResultMessage
// @Failure      500  {object}  gndstructservice.HTTPResultMessage
// @Router       /title [POST]
func (ctrl *Controller) getTitle(c *gin.Context) {
	var requestBody = &marc21struct.QueryStruct{}
	if err := c.BindJSON(requestBody); err != nil {
		NewResultMessage(c, http.StatusBadRequest, err)
		return
	}
	result, err := title(requestBody, ctrl.gnd)
	if err != nil {
		NewResultMessage(c, http.StatusInternalServerError, err)
		return
	}
	if result == nil {
		result = []any{}
	}
	if c.Query("format") == "xml" {
		c.XML(http.StatusOK, result)
		return
	}
	c.JSON(http.StatusOK, result)
}

// newMediaItem godoc
// @Summary      title from subject enrichment
// @Description  Takes the relevant MARC data of an entry and returns an enriched title structure
// @Security 	 ApiKeyAuth
// @Tags         GND
// @Produce      json
// @Param	     MarcQueryStruct	body		marc21struct.QueryStruct	true	"Get "
// @Success      200  {object}  []any
// @Failure      400  {object}  gndstructservice.HTTPResultMessage
// @Failure      404  {object}  gndstructservice.HTTPResultMessage
// @Failure      500  {object}  gndstructservice.HTTPResultMessage
// @Router       /subject.title [POST]
func (ctrl *Controller) getTitleSubject(c *gin.Context) {
	var requestBody = &marc21struct.QueryStruct{}
	if err := c.BindJSON(requestBody); err != nil {
		NewResultMessage(c, http.StatusBadRequest, err)
		return
	}
	result, err := subjectTitle(requestBody, ctrl.gnd)
	if err != nil {
		NewResultMessage(c, http.StatusInternalServerError, err)
		return
	}
	if result == nil {
		result = []any{}
	}
	if c.Query("format") == "xml" {
		c.XML(http.StatusOK, result)
		return
	}
	c.JSON(http.StatusOK, result)
}

// newMediaItem godoc
// @Summary      topic enrichment
// @Description  Takes the relevant MARC data of an entry and returns an enriched topic structure
// @Security 	 ApiKeyAuth
// @Tags         GND
// @Produce      json
// @Param	     MarcQueryStruct	body		marc21struct.QueryStruct	true	"Get "
// @Success      200  {object}  []any
// @Failure      400  {object}  gndstructservice.HTTPResultMessage
// @Failure      404  {object}  gndstructservice.HTTPResultMessage
// @Failure      500  {object}  gndstructservice.HTTPResultMessage
// @Router       /topic [POST]
func (ctrl *Controller) getTopic(c *gin.Context) {
	var requestBody = &marc21struct.QueryStruct{}
	if err := c.BindJSON(requestBody); err != nil {
		NewResultMessage(c, http.StatusBadRequest, err)
		return
	}
	result, err := topic(requestBody, ctrl.gnd)
	if err != nil {
		NewResultMessage(c, http.StatusInternalServerError, err)
		return
	}
	if result == nil {
		result = []any{}
	}
	if c.Query("format") == "xml" {
		c.XML(http.StatusOK, result)
		return
	}
	c.JSON(http.StatusOK, result)
}
