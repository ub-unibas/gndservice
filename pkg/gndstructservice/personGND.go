package gndstructservice

import "gitlab.switch.ch/ub-unibas/gndservice/v2/pkg/marc21struct"

type gndPersonResult struct {
	Level         string   `json:"level,omitempty"`
	Type          []string `json:"entityType,omitempty"`
	UseFor        []string `json:"usefor,omitempty"`
	Variant       []string `json:"variant,omitempty"`
	Gender        string   `json:"gender,omitempty"`
	Related       []string `json:"related,omitempty"`
	OtherId       []string `json:"otherIdentifier,omitempty"`
	PlaceBirth    []string `json:"placeOfBirth,omitempty"`
	PlaceActivity []string `json:"placeOfActivity,omitempty"`
	Profession    []string `json:"profession,omitempty"`
}

func getSubfield(sfs []*marc21struct.Subfield, code string) []string {
	var result = []string{}
	for _, sf := range sfs {
		if sf.Code == code {
			result = append(result, sf.Text)
		}
	}
	return result
}

func parseGNDPerson(rec *marc21struct.Record) *gndPersonResult {
	var result = &gndPersonResult{
		Level:         "",
		Type:          []string{},
		UseFor:        []string{},
		Variant:       []string{},
		Gender:        "",
		Related:       []string{},
		OtherId:       []string{},
		PlaceBirth:    []string{},
		PlaceActivity: []string{},
		Profession:    []string{},
	}
	for _, df := range rec.Datafields {
		switch df.Tag {
		case "024":
			source := getSubfield(df.Subfields, "2")
			id := getSubfield(df.Subfields, "a")
			if len(source) == 1 && len(id) == 1 {
				for index, s := range source {
					if s != "gnd" {
						result.OtherId = append(result.OtherId, "("+s+")"+id[index])
					}
				}
			}
		case "042":
			levels := getSubfield(df.Subfields, "a")
			if len(levels) > 0 {
				result.Level = levels[0]
			}
		case "075":
			result.Type = append(result.Type, getSubfield(df.Subfields, "b")...)
		case "079":
			result.UseFor = append(result.UseFor, getSubfield(df.Subfields, "q")...)
		case "400":
			result.Variant = append(result.Variant, getSubfield(df.Subfields, "a")...)
			result.Variant = append(result.Variant, getSubfield(df.Subfields, "b")...)
			result.Variant = append(result.Variant, getSubfield(df.Subfields, "c")...)
			result.Variant = append(result.Variant, getSubfield(df.Subfields, "q")...)
		case "375":
			genders := getSubfield(df.Subfields, "a")
			if len(genders) > 0 {
				result.Gender = genders[0]
			}
		case "500":
			relations := getSubfield(df.Subfields, "4")
			for _, r := range relations {
				if r == "nawi" || r == "pseu" {
					result.Related = append(result.Related, getSubfield(df.Subfields, "a")...)
					result.Related = append(result.Related, getSubfield(df.Subfields, "b")...)
					result.Related = append(result.Related, getSubfield(df.Subfields, "c")...)
					result.Related = append(result.Related, getSubfield(df.Subfields, "q")...)
				}
				if r == "beru" || r == "berc" {
					result.Profession = append(result.Profession, getSubfield(df.Subfields, "a")...)
				}
			}
		case "550":
			relations := getSubfield(df.Subfields, "4")
			for _, r := range relations {
				if r == "beru" || r == "berc" {
					result.Profession = append(result.Profession, getSubfield(df.Subfields, "a")...)
				}
			}
		case "551":
			relations := getSubfield(df.Subfields, "4")
			for _, r := range relations {
				if r == "ortg" {
					result.PlaceBirth = append(result.PlaceBirth, getSubfield(df.Subfields, "a")...)
				}
				if r == "ortw" {
					result.PlaceActivity = append(result.PlaceActivity, getSubfield(df.Subfields, "a")...)
				}
			}
		}
	}
	if len(result.Variant) == 0 {
		result.Variant = nil
	}
	if len(result.UseFor) == 0 {
		result.UseFor = nil
	}
	if len(result.Type) == 0 {
		result.Type = nil
	}
	if len(result.Related) == 0 {
		result.Related = nil
	}
	gen, ok := gender[result.Gender]
	if ok {
		result.Gender = gen
	}

	return result
}
