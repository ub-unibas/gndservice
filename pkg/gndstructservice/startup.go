package gndstructservice

import (
	"crypto/tls"
	ublogger "gitlab.switch.ch/ub-unibas/go-ublogger/v2"
	"net/http"
	"net/url"
	"sync"
)

type ShutdownService interface {
	Stop()
	GracefulStop()
}

func Startup(
	domain string,
	httpPort int,
	proxyUri string,
	tlsConfig *tls.Config,
	gndServiceUrl *url.URL,
	client *http.Client,
	wg *sync.WaitGroup,
	logger *ublogger.Logger) (ShutdownService, error) {
	ctrl, err := NewController(domain, httpPort, proxyUri, tlsConfig, gndServiceUrl, client)
	if err != nil {
		logger.Panicf("Could not start controller: %v", err)
	}
	ctrl.Start(wg, tlsConfig != nil)
	logger.Info().Msgf("server (%s) started: %s/swagger/index.html", ctrl.server.Addr, proxyUri)
	return ctrl, nil
}
