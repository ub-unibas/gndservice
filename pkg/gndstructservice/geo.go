package gndstructservice

import (
	"emperror.dev/errors"
	"fmt"
	"gitlab.switch.ch/ub-unibas/gndservice/v2/pkg/marc21struct"
	"strings"
)

type Geo struct {
	Name        string              `json:"namePart"`
	Description []string            `json:"description,omitempty"`
	Role        []string            `json:"role,omitempty"`
	Identifier  string              `json:"identifier,omitempty"`
	Level       string              `json:"level,omitempty"`
	Type        []string            `json:"entityType,omitempty"`
	UseFor      []string            `json:"useFor,omitempty"`
	Variant     []string            `json:"variant,omitempty"`
	Related     []string            `json:"related,omitempty"`
	Coordinates []gndGeoCoordinates `json:"coordinates,omitempty"`
	GeoNamesId  []string            `json:"geoNamesId,omitempty"`
	OtherId     []string            `json:"otherIdentifier,omitempty"`
}

type GeoResult map[string]Geo

func getGeoGND(identifier string, client *gnd) (*gndGeoResult, error) {
	rec, err := client.getGND(identifier)
	if err != nil {
		return nil, errors.Wrapf(err, "cannot get gnd '%s'", identifier)
	}
	gndData := parseGNDGeo(rec)
	return gndData, nil
}

func geo(rec *marc21struct.QueryStruct, client *gnd) ([]any, error) {
	if rec == nil {
		return nil, errors.New("empty query struct")
	}
	if rec.Datafields == nil {
		return nil, errors.New("no datafield in query struct")
	}
	var objects = map[string][]*marc21struct.Datafield{}
	for _, object := range rec.Datafields {
		if object == nil {
			return nil, errors.New("nil datafield in query struct")
		}

		if _, ok := objects[object.Tag]; !ok {
			objects[object.Tag] = []*marc21struct.Datafield{}
		}
		objects[object.Tag] = append(objects[object.Tag], object)
	}

	var allResult = []GeoResult{}
	var geoFields = []*marc21struct.Datafield{}
	field880, ok := objects["880"]
	if !ok {
		field880 = nil
	}
	field751, ok := objects["751"]
	if ok {
		geoFields = append(geoFields, field751...)
	}
	for _, field := range geoFields {
		var result = GeoResult{}
		var linkedField string
		var name string
		var description []string
		var role []string
		var identifier string
		var authority string
		for _, subField := range field.Subfields {
			switch subField.Code {
			case "a":
				name = subField.Text
			case "g":
				description = append(description, subField.Text)
			case "0":
				if strings.HasPrefix(subField.Text, "(") {
					identifier = subField.Text
				}
			case "4":
				role = append(role, subField.Text)
			}
		}
		for prefix, a := range authorityPrefixes {
			if strings.HasPrefix(identifier, prefix) {
				authority = a
			}
		}
		if authority == "" {
			authority = "unknown"
		}
		var gndData = &gndGeoResult{
			Level:       "",
			UseFor:      []string{},
			Variant:     []string{},
			Coordinates: []gndGeoCoordinates{},
			GeoNamesId:  []string{},
			OtherId:     []string{},
		}
		if authority == "gnd" && identifier != "" {
			var err error
			gndData, err = getGeoGND(identifier, client)
			if err != nil {
				fmt.Printf("could not find gnd '%s': %v", identifier, err)
				// we only accept not found errors
				if !errors.Is(err, errorNotFound) {
					return nil, errors.Wrapf(err, "could not get geo identifier '%s'", identifier)
				}
			}
		}
		if name != "" {
			if authority != "gnd" || gndData == nil {
				gndData = &gndGeoResult{
					Level:       "",
					UseFor:      []string{},
					Variant:     []string{},
					Coordinates: []gndGeoCoordinates{},
					GeoNamesId:  []string{},
					OtherId:     []string{},
				}
			}
			result[authority] = Geo{
				Name:        name,
				Description: description,
				Role:        role,
				Identifier:  identifier,
				Level:       gndData.Level,
				Type:        gndData.Type,
				UseFor:      gndData.UseFor,
				Variant:     gndData.Variant,
				Coordinates: gndData.Coordinates,
				GeoNamesId:  gndData.GeoNamesId,
				OtherId:     gndData.OtherId,
			}
		}
		if strings.HasPrefix(linkedField, "880-") {
			linkVal := strings.TrimPrefix(linkedField, "880-")
			for _, field := range field880 {
				var name string
				var description []string
				var role []string
				var identifier string
				var authority string
				var found bool
				for _, subField := range field.Subfields {
					if subField.Code == "6" {
						if strings.HasPrefix(subField.Text, "751-") {
							if subField.Text[4:6] == linkVal {
								found = true
							}
						}
					}
				}
				if !found {
					continue
				}
				for _, subField := range field.Subfields {
					switch subField.Code {
					case "a":
						name = subField.Text
					case "g":
						description = append(description, subField.Text)
					case "0":
						if strings.HasPrefix(subField.Text, "(") {
							identifier = subField.Text
						}
					case "4":
						role = append(role, subField.Text)
					}
				}
				for prefix, a := range authorityPrefixes {
					if strings.HasPrefix(identifier, prefix) {
						authority = a
					}
				}
				if authority == "" {
					authority = "alternateRepresentation"
				}
				if authority == "gnd" && identifier != "" {
					var err error
					gndData, err = getGeoGND(identifier, client)
					if err != nil {
						fmt.Printf("could not find gnd '%s': %v", identifier, err)
						// we only accept not found errors
						if !errors.Is(err, errorNotFound) {
							return nil, errors.Wrapf(err, "could not get geo identifier '%s'", identifier)
						}
					}
				}

				if name != "" && gndData != nil {
					if authority != "gnd" || gndData == nil {
						gndData = &gndGeoResult{}
					}
					result[authority] = Geo{
						Name:        name,
						Description: description,
						Role:        role,
						Identifier:  identifier,
						Level:       gndData.Level,
						Type:        gndData.Type,
						UseFor:      gndData.UseFor,
						Variant:     gndData.Variant,
						Coordinates: gndData.Coordinates,
						GeoNamesId:  gndData.GeoNamesId,
						OtherId:     gndData.OtherId,
					}
				}
			}
		}
		if len(result) != 0 {
			allResult = append(allResult, result)
		}
	}
	if len(allResult) == 0 {
		return nil, nil
	}
	ar := []any{}
	for _, v := range allResult {
		ar = append(ar, v)
	}
	return ar, nil
}

func subjectGeo(rec *marc21struct.QueryStruct, client *gnd) ([]any, error) {
	if rec == nil {
		return nil, errors.New("empty query struct")
	}
	if rec.Datafields == nil {
		return nil, errors.New("no datafield in query struct")
	}
	var objects = map[string][]*marc21struct.Datafield{}
	for _, object := range rec.Datafields {
		if object == nil {
			return nil, errors.New("nil datafield in query struct")
		}

		if _, ok := objects[object.Tag]; !ok {
			objects[object.Tag] = []*marc21struct.Datafield{}
		}
		objects[object.Tag] = append(objects[object.Tag], object)
	}

	var allResult = []GeoResult{}
	var subjectFields = []*marc21struct.Datafield{}
	field651, ok := objects["651"]
	if ok {
		subjectFields = append(subjectFields, field651...)
	}
	for _, field := range subjectFields {
		var result = GeoResult{}
		var name string
		var description []string
		var identifier string
		var authority string
		if len(result) != 0 {
			allResult = append(allResult, result)
		}
		for _, subField := range field.Subfields {
			var ok bool
			if field.Ind1 != "3" && subField.Code != "t" {
				ok = true
			}
			if ok {
				switch subField.Code {
				case "a":
					name = subField.Text
				case "g":
					description = append(description, subField.Text)
				case "0":
					if strings.HasPrefix(subField.Text, "(") {
						identifier = subField.Text
					}
				}
				switch field.Ind2 {
				case "0":
					authority = "lcsh"
				case "2":
					authority = "mesh"
				case "7":
					for _, subfield := range field.Subfields {
						if subfield.Code == "2" {
							switch subfield.Text {
							case "gnd":
								authority = "gnd"
							case "idsbb":
								authority = "idsbb"
							case "idref":
								authority = "idref"
							}
						}
					}
				}
			}
		}
		if authority == "" {
			authority = "other"
		}
		var gndData = &gndGeoResult{
			Level:       "",
			Type:        []string{},
			UseFor:      []string{},
			Variant:     []string{},
			Related:     []string{},
			Coordinates: []gndGeoCoordinates{},
			GeoNamesId:  []string{},
			OtherId:     []string{},
		}
		if authority == "gnd" && identifier != "" {
			var err error
			gndData, err = getGeoGND(identifier, client)
			if err != nil {
				fmt.Printf("could not find gnd '%s': %v", identifier, err)
				// we only accept not found errors
				if !errors.Is(err, errorNotFound) {
					return nil, errors.Wrapf(err, "could not get subject identifier '%s'", identifier)
				}
			}
		}
		if name != "" {
			if authority != "gnd" || gndData == nil {
				gndData = &gndGeoResult{
					Level:       "",
					Type:        []string{},
					UseFor:      []string{},
					Variant:     []string{},
					Related:     []string{},
					Coordinates: []gndGeoCoordinates{},
					GeoNamesId:  []string{},
					OtherId:     []string{},
				}
			}
			result[authority] = Geo{
				Name:        name,
				Description: description,
				Identifier:  identifier,
				Level:       gndData.Level,
				Type:        gndData.Type,
				UseFor:      gndData.UseFor,
				Variant:     gndData.Variant,
				Related:     gndData.Related,
				Coordinates: gndData.Coordinates,
				GeoNamesId:  gndData.GeoNamesId,
				OtherId:     gndData.OtherId,
			}
		}
		if len(result) != 0 {
			allResult = append(allResult, result)
		}
	}
	if len(allResult) == 0 {
		return nil, nil
	}
	ar := []any{}
	for _, v := range allResult {
		ar = append(ar, v)
	}
	return ar, nil
}
