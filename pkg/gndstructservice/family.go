package gndstructservice

import (
	"emperror.dev/errors"
	"fmt"
	"gitlab.switch.ch/ub-unibas/gndservice/v2/pkg/marc21struct"
	"regexp"
	"strings"
)

type Family struct {
	Name       string   `json:"namePart"`
	Date       string   `json:"date,omitempty"`
	Role       []string `json:"role,omitempty"`
	Identifier string   `json:"identifier,omitempty"`
	Level      string   `json:"level,omitempty"`
	Type       []string `json:"entityType,omitempty"`
	UseFor     []string `json:"useFor,omitempty"`
	Variant    []string `json:"variant,omitempty"`
	OtherId    []string `json:"otherIdentifier,omitempty"`
}

type FamilyResult map[string]Family

func getFamilyGND(identifier string, client *gnd) (*gndFamilyResult, error) {
	rec, err := client.getGND(identifier)
	if err != nil {
		return nil, errors.Wrapf(err, "cannot get gnd '%s'", identifier)
	}
	gndData := parseGNDFamily(rec)
	return gndData, nil
}

func family(rec *marc21struct.QueryStruct, client *gnd) ([]any, error) {
	if rec == nil {
		return nil, errors.New("empty query struct")
	}
	var id string
	var source string
	for _, field := range rec.Datafields {
		if field.Tag == "035" {
			for _, sub := range field.Subfields {
				if sub.Code == "a" {
					id = sub.Text
					if strings.Contains(sub.Text, "EXLCZ") {
						source = "cz"
					}
				}
			}
		}
	}
	_ = id
	_ = source
	if rec.Datafields == nil {
		return nil, errors.New("no datafield in query struct")
	}
	var objects = map[string][]*marc21struct.Datafield{}
	for _, object := range rec.Datafields {
		if object == nil {
			return nil, errors.New("nil datafield in query struct")
		}

		if _, ok := objects[object.Tag]; !ok {
			objects[object.Tag] = []*marc21struct.Datafield{}
		}
		objects[object.Tag] = append(objects[object.Tag], object)
	}

	var allResult = []FamilyResult{}
	var fields = []*marc21struct.Datafield{}
	field880, ok := objects["880"]
	if !ok {
		field880 = nil
	}
	field700, ok := objects["700"]
	if ok {
		for _, fld := range field700 {
			var doNotUse bool

			// only 700 3# are families
			if fld.Ind1 != "3" {
				doNotUse = true
			}

			// 700  $$t works are no families
			for _, sub := range fld.Subfields {
				if sub.Code == "t" {
					doNotUse = true
					break
				}
			}
			if !doNotUse {
				fields = append(fields, fld)
			}
		}
	}
	field100, ok := objects["100"]
	if ok {
		for _, fld := range field100 {
			// only 100 3# are families
			if fld.Ind1 == "3" {
				fields = append(fields, fld)
			}
		}
	}
	for _, field := range fields {
		var result = FamilyResult{}
		var linkedField string
		var name string
		var date string
		var role []string
		var identifier string
		var authority string
		for _, subField := range field.Subfields {
			switch subField.Code {
			case "a":
				name = subField.Text
			case "b":
				name += " " + subField.Text
			case "c":
				trimmedName := strings.Trim(name, " ")
				if strings.HasSuffix(trimmedName, ",") {
					name += " " + subField.Text
				} else {
					name += ", " + subField.Text
				}
			case "d":
				if source == "cz" {
					// Remove parentheses at beginning and end, remove suffix .,;
					cleanDate := strings.TrimPrefix(subField.Text, "(")
					cleanDate = strings.TrimSuffix(cleanDate, ")")
					cleanDate = strings.TrimSuffix(cleanDate, ".")
					cleanDate = strings.TrimSuffix(cleanDate, ",")
					cleanDate = strings.TrimSuffix(cleanDate, ";")
					date = cleanDate
				} else {
					date = subField.Text
				}
			case "0":
				if strings.HasPrefix(subField.Text, "(") {
					identifier = subField.Text
				}
			case "4":
				role = append(role, subField.Text)
			case "6":
				linkedField = subField.Text
			}
		}
		for prefix, a := range authorityPrefixes {
			if strings.HasPrefix(identifier, prefix) {
				authority = a
			}
		}
		if authority == "" {
			authority = "unknown"
		}
		var gndData = &gndFamilyResult{
			Level:   "",
			Type:    []string{},
			UseFor:  []string{},
			Variant: []string{},
			OtherId: []string{},
		}
		if authority == "gnd" && identifier != "" {
			var err error
			gndData, err = getFamilyGND(identifier, client)
			if err != nil {
				fmt.Printf("could not find gnd '%s': %v", identifier, err)
				// we only accept not found errors
				if !errors.Is(err, errorNotFound) {
					return nil, errors.Wrapf(err, "could not get family identifier '%s'", identifier)
				}
			}
		}
		if name != "" {
			if authority != "gnd" || gndData == nil {
				gndData = &gndFamilyResult{
					Level:   "",
					Type:    []string{},
					UseFor:  []string{},
					Variant: []string{},
					OtherId: []string{},
				}
			}
			trimmedName := strings.Trim(name, " ")
			if strings.HasSuffix(trimmedName, ",") {
				name = strings.TrimSuffix(trimmedName, ",")
			}
			if source == "cz" && strings.HasSuffix(trimmedName, ".") {
				// remove dot only if it's preceeded by two lowercase letters
				regex := regexp.MustCompile(`[a-z]{2}\.$`)
				if regex.MatchString(trimmedName) {
					name = strings.TrimSuffix(trimmedName, ".")
				}
			}
			result[authority] = Family{
				Name:       name,
				Date:       date,
				Role:       role,
				Identifier: identifier,
				Level:      gndData.Level,
				Type:       gndData.Type,
				UseFor:     gndData.UseFor,
				Variant:    gndData.Variant,
				OtherId:    gndData.OtherId,
			}
		}
		if strings.HasPrefix(linkedField, "880-") {
			linkVal := strings.TrimPrefix(linkedField, "880-")
			for _, field := range field880 {
				var name string
				var date string
				var role []string
				var identifier string
				var authority string
				var found bool
				for _, subField := range field.Subfields {
					if subField.Code == "6" {
						if strings.HasPrefix(subField.Text, "100-") || strings.HasPrefix(subField.Text, "700-") {
							if subField.Text[4:6] == linkVal {
								found = true
							}
						}
					}
				}
				if !found {
					continue
				}
				for _, subField := range field.Subfields {
					switch subField.Code {
					case "a":
						name = subField.Text
					case "b":
						name += " " + subField.Text
					case "c":
						trimmedName := strings.Trim(name, " ")
						if strings.HasSuffix(trimmedName, ",") {
							name += " " + subField.Text
						} else {
							name += ", " + subField.Text
						}
					case "d":
						if source == "cz" {
							// Remove parentheses at beginning and end, remove suffix .,;
							cleanDate := strings.TrimPrefix(subField.Text, "(")
							cleanDate = strings.TrimSuffix(cleanDate, ")")
							cleanDate = strings.TrimSuffix(cleanDate, ".")
							cleanDate = strings.TrimSuffix(cleanDate, ",")
							cleanDate = strings.TrimSuffix(cleanDate, ";")
							date = cleanDate
						} else {
							date = subField.Text
						}
					case "0":
						if strings.HasPrefix(subField.Text, "(") {
							identifier = subField.Text
						}
					case "4":
						role = append(role, subField.Text)
					}
				}
				for prefix, a := range authorityPrefixes {
					if strings.HasPrefix(identifier, prefix) {
						authority = a
					}
				}
				if authority == "" {
					authority = "alternateRepresentation"
				}
				if authority == "gnd" && identifier != "" {
					var err error
					gndData, err = getFamilyGND(identifier, client)
					if err != nil {
						fmt.Printf("could not find gnd '%s': %v", identifier, err)
						// we only accept not found errors
						if !errors.Is(err, errorNotFound) {
							return nil, errors.Wrapf(err, "could not get family identifier '%s'", identifier)
						}
					}
				}

				if name != "" && gndData != nil {
					if authority != "gnd" || gndData == nil {
						gndData = &gndFamilyResult{}
					}
					trimmedName := strings.Trim(name, " ")
					if strings.HasSuffix(trimmedName, ",") {
						name = strings.TrimSuffix(trimmedName, ",")
					}
					if source == "cz" && strings.HasSuffix(trimmedName, ".") {
						// remove dot only if it's preceeded by two lowercase letters
						regex := regexp.MustCompile(`[a-z]{2}\.$`)
						if regex.MatchString(trimmedName) {
							name = strings.TrimSuffix(trimmedName, ".")
						}
					}
					result[authority] = Family{
						Name:       name,
						Date:       date,
						Role:       role,
						Identifier: identifier,
						Level:      gndData.Level,
						Type:       gndData.Type,
						UseFor:     gndData.UseFor,
						Variant:    gndData.Variant,
						OtherId:    gndData.OtherId,
					}
				}
			}
		}
		if len(result) != 0 {
			allResult = append(allResult, result)
		}
	}
	if len(allResult) == 0 {
		return nil, nil
	}
	ar := []any{}
	for _, v := range allResult {
		ar = append(ar, v)
	}
	return ar, nil
}

func subjectFamily(rec *marc21struct.QueryStruct, client *gnd) ([]any, error) {
	if rec == nil {
		return nil, errors.New("empty query struct")
	}
	var source string
	for _, field := range rec.Datafields {
		if field.Tag == "035" {
			for _, sub := range field.Subfields {
				if sub.Code == "a" {
					if strings.Contains(sub.Text, "EXLCZ") {
						source = "cz"
					}
				}
			}
		}
	}
	_ = source
	if rec.Datafields == nil {
		return nil, errors.New("no datafield in query struct")
	}
	var objects = map[string][]*marc21struct.Datafield{}
	for _, object := range rec.Datafields {
		if object == nil {
			return nil, errors.New("nil datafield in query struct")
		}

		if _, ok := objects[object.Tag]; !ok {
			objects[object.Tag] = []*marc21struct.Datafield{}
		}
		objects[object.Tag] = append(objects[object.Tag], object)
	}

	var allResult = []FamilyResult{}
	var subjectFields = []*marc21struct.Datafield{}
	field600, ok := objects["600"]
	if ok {
		for _, fld := range field600 {
			var doNotUse bool

			// only 600 3# are families
			if fld.Ind1 != "3" {
				doNotUse = true
			}

			// 600  $$t works are no families
			for _, sub := range fld.Subfields {
				if sub.Code == "t" {
					doNotUse = true
					break
				}
			}
			if !doNotUse {
				subjectFields = append(subjectFields, fld)
			}
		}
	}
	for _, field := range subjectFields {
		var result = FamilyResult{}
		var name string
		var date string
		var identifier string
		var authority string
		if len(result) != 0 {
			allResult = append(allResult, result)
		}
		for _, subField := range field.Subfields {
			switch subField.Code {
			case "a":
				name = subField.Text
			case "b":
				name += " " + subField.Text
			case "c":
				trimmedName := strings.Trim(name, " ")
				if strings.HasSuffix(trimmedName, ",") {
					name += " " + subField.Text
				} else {
					name += ", " + subField.Text
				}
			case "d":
				if source == "cz" {
					// Remove parentheses at beginning and end, remove suffix .,;
					cleanDate := strings.TrimPrefix(subField.Text, "(")
					cleanDate = strings.TrimSuffix(cleanDate, ")")
					cleanDate = strings.TrimSuffix(cleanDate, ".")
					cleanDate = strings.TrimSuffix(cleanDate, ",")
					cleanDate = strings.TrimSuffix(cleanDate, ";")
					date = cleanDate
				} else {
					date = subField.Text
				}
			case "0":
				if strings.HasPrefix(subField.Text, "(") {
					identifier = subField.Text
				}
			}
		}
		switch field.Ind2 {
		case "0":
			authority = "lcsh"
		case "2":
			authority = "mesh"
		case "7":
			for _, subfield := range field.Subfields {
				if subfield.Code == "2" {
					switch subfield.Text {
					case "gnd":
						authority = "gnd"
					case "idsbb":
						authority = "idsbb"
					case "idref":
						authority = "idref"
					}
				}
			}
		}
		if authority == "" {
			authority = "other"
		}
		var gndData = &gndFamilyResult{
			Level:   "",
			Type:    []string{},
			UseFor:  []string{},
			Variant: []string{},
			OtherId: []string{},
		}
		if authority == "gnd" && identifier != "" {
			var err error
			gndData, err = getFamilyGND(identifier, client)
			if err != nil {
				fmt.Printf("could not find gnd '%s': %v", identifier, err)
				// we only accept not found errors
				if !errors.Is(err, errorNotFound) {
					return nil, errors.Wrapf(err, "could not get family subject identifier '%s'", identifier)
				}
			}
		}
		if name != "" {
			if authority != "gnd" || gndData == nil {
				gndData = &gndFamilyResult{
					Level:   "",
					Type:    []string{},
					UseFor:  []string{},
					Variant: []string{},
					OtherId: []string{},
				}
			}
			trimmedName := strings.Trim(name, " ")
			if strings.HasSuffix(trimmedName, ",") {
				name = strings.TrimSuffix(trimmedName, ",")
			}
			if source == "cz" && strings.HasSuffix(trimmedName, ".") {
				// remove dot only if it's preceeded by two lowercase letters
				regex := regexp.MustCompile(`[a-z]{2}\.$`)
				if regex.MatchString(trimmedName) {
					name = strings.TrimSuffix(trimmedName, ".")
				}
			}
			result[authority] = Family{
				Name:       name,
				Date:       date,
				Identifier: identifier,
				Level:      gndData.Level,
				Type:       gndData.Type,
				UseFor:     gndData.UseFor,
				Variant:    gndData.Variant,
				OtherId:    gndData.OtherId,
			}
		}
		if len(result) != 0 {
			allResult = append(allResult, result)
		}
	}
	if len(allResult) == 0 {
		return nil, nil
	}
	ar := []any{}
	for _, v := range allResult {
		ar = append(ar, v)
	}
	return ar, nil
}
