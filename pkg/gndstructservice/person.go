package gndstructservice

import (
	"emperror.dev/errors"
	"fmt"
	"gitlab.switch.ch/ub-unibas/gndservice/v2/pkg/marc21struct"
	"regexp"
	"strings"
)

var gender = map[string]string{
	"0": "not known",
	"1": "male",
	"2": "female",
	"9": "not applicable",
}

type Person struct {
	Name          string   `json:"namePart"`
	Date          string   `json:"date,omitempty"`
	Role          []string `json:"role,omitempty"`
	Identifier    string   `json:"identifier,omitempty"`
	Level         string   `json:"level,omitempty"`
	Type          []string `json:"entityType,omitempty"`
	UseFor        []string `json:"useFor,omitempty"`
	Variant       []string `json:"variant,omitempty"`
	Gender        string   `json:"gender,omitempty"`
	Related       []string `json:"related,omitempty"`
	OtherId       []string `json:"otherIdentifier,omitempty"`
	PlaceBirth    []string `json:"placeOfBirth,omitempty"`
	PlaceActivity []string `json:"placeOfActivity,omitempty"`
	Profession    []string `json:"profession,omitempty"`
}

type PersonResult map[string]Person

var authorityPrefixes = map[string]string{
	"(DE-588)": "gnd",
	"(IDREF)":  "idref",
	"(IdRef)":  "idref",
	"(RERO)":   "rero",
	"(SBT11)":  "sbt",
}

var gndCount int64

func getPersonGND(identifier string, client *gnd) (*gndPersonResult, error) {
	rec, err := client.getGND(identifier)
	if err != nil {
		return nil, errors.Wrapf(err, "cannot get gnd '%s'", identifier)
	}
	gndData := parseGNDPerson(rec)
	return gndData, nil
}

func person(rec *marc21struct.QueryStruct, client *gnd) ([]any, error) {
	if rec == nil {
		return nil, errors.New("empty query struct")
	}
	var id string
	var source string
	for _, field := range rec.Datafields {
		if field.Tag == "035" {
			for _, sub := range field.Subfields {
				if sub.Code == "a" {
					id = sub.Text
					if strings.Contains(sub.Text, "EXLCZ") {
						source = "cz"
					}
				}
			}
		}
	}
	_ = id
	_ = source
	if rec.Datafields == nil {
		return nil, errors.New("no datafield in query struct")
	}
	var objects = map[string][]*marc21struct.Datafield{}
	for _, object := range rec.Datafields {
		if object == nil {
			return nil, errors.New("nil datafield in query struct")
		}

		if _, ok := objects[object.Tag]; !ok {
			objects[object.Tag] = []*marc21struct.Datafield{}
		}
		objects[object.Tag] = append(objects[object.Tag], object)
	}

	var allResult = []PersonResult{}
	var personFields = []*marc21struct.Datafield{}

	field880, ok := objects["880"]
	if !ok {
		field880 = nil
	}

	// all person relevant fields into personFields list
	field700, ok := objects["700"]
	if ok {
		for _, fld := range field700 {
			var doNotUse bool

			// 700 3# families are no persons
			if fld.Ind1 == "3" {
				doNotUse = true
			}

			// 700  $$t works are no persons
			for _, sub := range fld.Subfields {
				if sub.Code == "t" {
					doNotUse = true
					break
				}
			}
			if !doNotUse {
				personFields = append(personFields, fld)
			}
		}
	}
	field100, ok := objects["100"]
	if ok {
		for _, fld := range field100 {
			// 100 3# families are no persons
			if fld.Ind1 != "3" {
				personFields = append(personFields, fld)
			}

		}
	}

	// iterate all person relevant fields
	for _, field := range personFields {
		var result = PersonResult{}
		var linkedField string
		var name string
		var date string
		var role []string
		var identifier string
		var authority string

		for _, subField := range field.Subfields {
			switch subField.Code {
			case "a":
				name = subField.Text
			case "b":
				name += " " + subField.Text
			case "c":
				trimmedName := strings.Trim(name, " ")
				if strings.HasSuffix(trimmedName, ",") {
					name += " " + subField.Text
				} else {
					name += ", " + subField.Text
				}
			case "d":
				if source == "cz" {
					// Remove parentheses at beginning and end, remove suffix .,;
					cleanDate := strings.TrimSuffix(subField.Text, ".")
					cleanDate = strings.TrimSuffix(cleanDate, ",")
					cleanDate = strings.TrimSuffix(cleanDate, ";")
					cleanDate = strings.TrimPrefix(cleanDate, "(")
					cleanDate = strings.TrimSuffix(cleanDate, ")")
					date = cleanDate
				} else {
					date = subField.Text
				}
			case "0":
				if strings.HasPrefix(subField.Text, "(") {
					identifier = subField.Text
				}
			case "4":
				role = append(role, subField.Text)
			case "6":
				linkedField = subField.Text
			}
		}
		for prefix, a := range authorityPrefixes {
			if strings.HasPrefix(identifier, prefix) {
				authority = a
			}
		}
		if authority == "" {
			authority = "unknown"
		}
		var gndData = &gndPersonResult{
			Level:         "",
			Type:          []string{},
			UseFor:        []string{},
			Variant:       []string{},
			Gender:        "",
			OtherId:       []string{},
			PlaceBirth:    []string{},
			PlaceActivity: []string{},
			Profession:    []string{},
		}
		if authority == "gnd" && identifier != "" {
			var err error
			gndData, err = getPersonGND(identifier, client)
			if err != nil {
				fmt.Printf("could not find gnd '%s': %v", identifier, err)
				// we only accept not found errors
				if !errors.Is(err, errorNotFound) {
					return nil, errors.Wrapf(err, "could not get person identifier '%s'", identifier)
				}
			}
		}
		if name != "" {
			if authority != "gnd" || gndData == nil {
				gndData = &gndPersonResult{
					Level:         "",
					Type:          []string{},
					UseFor:        []string{},
					Variant:       []string{},
					Gender:        "",
					OtherId:       []string{},
					PlaceBirth:    []string{},
					PlaceActivity: []string{},
					Profession:    []string{},
				}
			}
			trimmedName := strings.Trim(name, " ")
			if strings.HasSuffix(trimmedName, ",") {
				name = strings.TrimSuffix(trimmedName, ",")
			}
			if source == "cz" && strings.HasSuffix(trimmedName, ".") {
				// remove dot only if it's preceeded by two lowercase letters
				regex := regexp.MustCompile(`[a-z]{2}\.$`)
				if regex.MatchString(trimmedName) {
					name = strings.TrimSuffix(trimmedName, ".")
				}
			}
			result[authority] = Person{
				Name:          name,
				Date:          date,
				Role:          role,
				Identifier:    identifier,
				Level:         gndData.Level,
				Type:          gndData.Type,
				UseFor:        gndData.UseFor,
				Variant:       gndData.Variant,
				Gender:        gndData.Gender,
				OtherId:       gndData.OtherId,
				PlaceBirth:    gndData.PlaceBirth,
				PlaceActivity: gndData.PlaceActivity,
				Profession:    gndData.Profession,
			}
		}
		if strings.HasPrefix(linkedField, "880-") {
			linkVal := strings.TrimPrefix(linkedField, "880-")
			for _, field := range field880 {
				var name string
				var date string
				var role []string
				var identifier string
				var authority string
				var found bool
				for _, subField := range field.Subfields {
					if subField.Code == "6" {
						if strings.HasPrefix(subField.Text, "100-") || strings.HasPrefix(subField.Text, "700-") {
							if subField.Text[4:6] == linkVal {
								found = true
							}
						}
					}
				}
				if !found {
					continue
				}
				for _, subField := range field.Subfields {
					switch subField.Code {
					case "a":
						name = subField.Text
					case "b":
						name += " " + subField.Text
					case "c":
						trimmedName := strings.Trim(name, " ")
						if strings.HasSuffix(trimmedName, ",") {
							name += " " + subField.Text
						} else {
							name += ", " + subField.Text
						}
					case "d":
						if source == "cz" {
							// Remove parentheses at beginning and end, remove suffix .,;
							cleanDate := strings.TrimSuffix(subField.Text, ".")
							cleanDate = strings.TrimSuffix(cleanDate, ",")
							cleanDate = strings.TrimSuffix(cleanDate, ";")
							cleanDate = strings.TrimPrefix(cleanDate, "(")
							cleanDate = strings.TrimSuffix(cleanDate, ")")
							date = cleanDate
						} else {
							date = subField.Text
						}
					case "0":
						if strings.HasPrefix(subField.Text, "(") {
							identifier = subField.Text
						}
					case "4":
						role = append(role, subField.Text)
					}
				}
				for prefix, a := range authorityPrefixes {
					if strings.HasPrefix(identifier, prefix) {
						authority = a
					}
				}
				if authority == "" {
					authority = "alternateRepresentation"
				}
				if authority == "gnd" && identifier != "" {
					var err error
					gndData, err = getPersonGND(identifier, client)
					if err != nil {
						fmt.Printf("could not find gnd '%s': %v", identifier, err)
						// we only accept not found errors
						if !errors.Is(err, errorNotFound) {
							return nil, errors.Wrapf(err, "could not get person identifier '%s'", identifier)
						}
					}
				}

				if name != "" && gndData != nil {
					if authority != "gnd" || gndData == nil {
						gndData = &gndPersonResult{}
					}
					trimmedName := strings.Trim(name, " ")
					if strings.HasSuffix(trimmedName, ",") {
						name = strings.TrimSuffix(trimmedName, ",")
					}
					if source == "cz" && strings.HasSuffix(trimmedName, ".") {
						// remove dot only if it's preceeded by two lowercase letters
						regex := regexp.MustCompile(`[a-z]{2}\.$`)
						if regex.MatchString(trimmedName) {
							name = strings.TrimSuffix(trimmedName, ".")
						}
					}
					result[authority] = Person{
						Name:          name,
						Date:          date,
						Role:          role,
						Identifier:    identifier,
						Level:         gndData.Level,
						Type:          gndData.Type,
						UseFor:        gndData.UseFor,
						Variant:       gndData.Variant,
						Gender:        gndData.Gender,
						OtherId:       gndData.OtherId,
						PlaceBirth:    gndData.PlaceBirth,
						PlaceActivity: gndData.PlaceActivity,
						Profession:    gndData.Profession,
					}
				}
			}
		}
		if len(result) != 0 {
			allResult = append(allResult, result)
		}
	}
	if len(allResult) == 0 {
		return nil, nil
	}
	ar := []any{}
	for _, v := range allResult {
		ar = append(ar, v)
	}
	return ar, nil
}

func subjectPerson(rec *marc21struct.QueryStruct, client *gnd) ([]any, error) {
	if rec == nil {
		return nil, errors.New("empty query struct")
	}
	var source string
	for _, field := range rec.Datafields {
		if field.Tag == "035" {
			for _, sub := range field.Subfields {
				if sub.Code == "a" {
					if strings.Contains(sub.Text, "EXLCZ") {
						source = "cz"
					}
				}
			}
		}
	}
	_ = source
	if rec.Datafields == nil {
		return nil, errors.New("no datafield in query struct")
	}
	var objects = map[string][]*marc21struct.Datafield{}
	for _, object := range rec.Datafields {
		if object == nil {
			return nil, errors.New("nil datafield in query struct")
		}

		if _, ok := objects[object.Tag]; !ok {
			objects[object.Tag] = []*marc21struct.Datafield{}
		}
		objects[object.Tag] = append(objects[object.Tag], object)
	}

	var allResult = []PersonResult{}
	var subjectFields = []*marc21struct.Datafield{}
	field600, ok := objects["600"]
	if ok {
		for _, fld := range field600 {
			var doNotUse bool

			// 600 3# families are no persons
			if fld.Ind1 == "3" {
				doNotUse = true
			}

			// 600  $$t works are no persons
			for _, sub := range fld.Subfields {
				if sub.Code == "t" {
					doNotUse = true
					break
				}
			}
			if !doNotUse {
				subjectFields = append(subjectFields, fld)
			}
		}
	}
	for _, field := range subjectFields {
		var result = PersonResult{}
		var name string
		var date string
		var identifier string
		var authority string
		if len(result) != 0 {
			allResult = append(allResult, result)
		}
		for _, subField := range field.Subfields {
			switch subField.Code {
			case "a":
				name = subField.Text
			case "b":
				name += " " + subField.Text
			case "c":
				trimmedName := strings.Trim(name, " ")
				if strings.HasSuffix(trimmedName, ",") {
					name += " " + subField.Text
				} else {
					name += ", " + subField.Text
				}
			case "d":
				if source == "cz" {
					// Remove parentheses at beginning and end, remove suffix .,;
					cleanDate := strings.TrimSuffix(subField.Text, ".")
					cleanDate = strings.TrimSuffix(cleanDate, ",")
					cleanDate = strings.TrimSuffix(cleanDate, ";")
					cleanDate = strings.TrimPrefix(cleanDate, "(")
					cleanDate = strings.TrimSuffix(cleanDate, ")")
					date = cleanDate
				} else {
					date = subField.Text
				}
			case "0":
				if strings.HasPrefix(subField.Text, "(") {
					identifier = subField.Text
				}
			}
		}
		switch field.Ind2 {
		case "0":
			authority = "lcsh"
		case "2":
			authority = "mesh"
		case "7":
			for _, subfield := range field.Subfields {
				if subfield.Code == "2" {
					switch subfield.Text {
					case "gnd":
						authority = "gnd"
					case "idsbb":
						authority = "idsbb"
					case "idref":
						authority = "idref"
					}
				}
			}
		}
		if authority == "" {
			authority = "other"
		}
		var gndData = &gndPersonResult{
			Level:         "",
			Type:          []string{},
			UseFor:        []string{},
			Variant:       []string{},
			Gender:        "",
			Related:       []string{},
			OtherId:       []string{},
			PlaceBirth:    []string{},
			PlaceActivity: []string{},
			Profession:    []string{},
		}
		if authority == "gnd" && identifier != "" {
			var err error
			gndData, err = getPersonGND(identifier, client)
			if err != nil {
				fmt.Printf("could not find gnd '%s': %v", identifier, err)
				// we only accept not found errors
				if !errors.Is(err, errorNotFound) {
					return nil, errors.Wrapf(err, "could not get person identifier '%s'", identifier)
				}
			}
		}
		if name != "" {
			if authority != "gnd" || gndData == nil {
				gndData = &gndPersonResult{
					Level:         "",
					Type:          []string{},
					UseFor:        []string{},
					Variant:       []string{},
					Gender:        "",
					Related:       []string{},
					OtherId:       []string{},
					PlaceBirth:    []string{},
					PlaceActivity: []string{},
					Profession:    []string{},
				}
			}
			trimmedName := strings.Trim(name, " ")
			if strings.HasSuffix(trimmedName, ",") {
				name = strings.TrimSuffix(trimmedName, ",")
			}
			if source == "cz" && strings.HasSuffix(trimmedName, ".") {
				// remove dot only if it's preceeded by two lowercase letters
				regex := regexp.MustCompile(`[a-z]{2}\.$`)
				if regex.MatchString(trimmedName) {
					name = strings.TrimSuffix(trimmedName, ".")
				}
			}
			result[authority] = Person{
				Name:          name,
				Date:          date,
				Identifier:    identifier,
				Level:         gndData.Level,
				Type:          gndData.Type,
				UseFor:        gndData.UseFor,
				Variant:       gndData.Variant,
				Gender:        gndData.Gender,
				Related:       gndData.Related,
				OtherId:       gndData.OtherId,
				PlaceBirth:    gndData.PlaceBirth,
				PlaceActivity: gndData.PlaceActivity,
				Profession:    gndData.Profession,
			}
		}
		if len(result) != 0 {
			allResult = append(allResult, result)
		}
	}
	if len(allResult) == 0 {
		return nil, nil
	}
	ar := []any{}
	for _, v := range allResult {
		ar = append(ar, v)
	}
	return ar, nil
}
