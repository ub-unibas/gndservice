package gndstructservice

import (
	"emperror.dev/errors"
	"fmt"
	"gitlab.switch.ch/ub-unibas/gndservice/v2/pkg/marc21struct"
	"regexp"
	"strings"
)

type Corporate struct {
	Name        string   `json:"namePart"`
	Description []string `json:"description,omitempty"`
	Role        []string `json:"role,omitempty"`
	Identifier  string   `json:"identifier,omitempty"`
	Level       string   `json:"level,omitempty"`
	UseFor      []string `json:"useFor,omitempty"`
	Type        []string `json:"entityType,omitempty"`
	Variant     []string `json:"variant,omitempty"`
	Related     []string `json:"related,omitempty"`
	OtherId     []string `json:"otherIdentifier,omitempty"`
	Place       []string `json:"placeOfBusiness,omitempty"`
}

type CorporateResult map[string]Corporate

func getCorporateGND(identifier string, client *gnd) (*gndCorporateResult, error) {
	rec, err := client.getGND(identifier)
	if err != nil {
		return nil, errors.Wrapf(err, "cannot get gnd '%s'", identifier)
	}
	gndData := parseGNDCorporate(rec)
	return gndData, nil
}

func corporate(rec *marc21struct.QueryStruct, client *gnd) ([]any, error) {
	if rec == nil {
		return nil, errors.New("empty query struct")
	}
	var id string
	var source string
	for _, field := range rec.Datafields {
		if field.Tag == "035" {
			for _, sub := range field.Subfields {
				if sub.Code == "a" {
					id = sub.Text
					if strings.Contains(sub.Text, "EXLCZ") {
						source = "cz"
					}
				}
			}
		}
	}
	_ = id
	_ = source
	if rec.Datafields == nil {
		return nil, errors.New("no datafield in query struct")
	}
	var objects = map[string][]*marc21struct.Datafield{}
	for _, object := range rec.Datafields {
		if object == nil {
			return nil, errors.New("nil datafield in query struct")
		}

		if _, ok := objects[object.Tag]; !ok {
			objects[object.Tag] = []*marc21struct.Datafield{}
		}
		objects[object.Tag] = append(objects[object.Tag], object)
	}

	var allResult = []CorporateResult{}
	var fields = []*marc21struct.Datafield{}
	field880, ok := objects["880"]
	if !ok {
		field880 = nil
	}
	field710, ok := objects["710"]
	if ok {
		for _, fld := range field710 {
			var doNotUse bool

			// 710  $$t works are no corporates
			for _, sub := range fld.Subfields {
				if sub.Code == "t" {
					doNotUse = true
					break
				}
			}
			if !doNotUse {
				fields = append(fields, fld)
			}
		}
	}
	field110, ok := objects["110"]
	if ok {
		fields = append(fields, field110...)
	}
	for _, field := range fields {
		var result = CorporateResult{}
		var linkedField string
		var name string
		var description []string
		var role []string
		var identifier string
		var authority string

		for _, subField := range field.Subfields {
			switch subField.Code {
			case "a":
				name = subField.Text
			case "b":
				trimmedName := strings.Trim(name, " ")
				if strings.HasSuffix(trimmedName, ",") {
					name += " " + subField.Text
				} else {
					name += ", " + subField.Text
				}
			case "g":
				if source == "cz" {
					// remove suffix .,;
					cleanDescription := strings.TrimSuffix(subField.Text, ".")
					cleanDescription = strings.TrimSuffix(cleanDescription, ",")
					cleanDescription = strings.TrimSuffix(cleanDescription, ";")
					description = append(description, cleanDescription)
				} else {
					description = append(description, subField.Text)
				}
				description = append(description, subField.Text)
			case "0":
				if strings.HasPrefix(subField.Text, "(") {
					identifier = subField.Text
				}
			case "4":
				role = append(role, subField.Text)
			case "6":
				linkedField = subField.Text
			}
		}
		for prefix, a := range authorityPrefixes {
			if strings.HasPrefix(identifier, prefix) {
				authority = a
			}
		}
		if authority == "" {
			authority = "unknown"
		}
		var gndData = &gndCorporateResult{
			Level:   "",
			Type:    []string{},
			UseFor:  []string{},
			Variant: []string{},
			OtherId: []string{},
			Place:   []string{},
		}
		if authority == "gnd" && identifier != "" {
			var err error
			gndData, err = getCorporateGND(identifier, client)
			if err != nil {
				fmt.Printf("could not find gnd '%s': %v", identifier, err)
				// we only accept not found errors
				if !errors.Is(err, errorNotFound) {
					return nil, errors.Wrapf(err, "could not get corporate identifier '%s'", identifier)
				}
			}
		}
		if name != "" {
			if authority != "gnd" || gndData == nil {
				gndData = &gndCorporateResult{
					Level:   "",
					Type:    []string{},
					UseFor:  []string{},
					Variant: []string{},
					OtherId: []string{},
					Place:   []string{},
				}
			}
			trimmedName := strings.Trim(name, " ")
			if strings.HasSuffix(trimmedName, ",") {
				name = strings.TrimSuffix(trimmedName, ",")
			}
			if source == "cz" && strings.HasSuffix(trimmedName, ".") {
				// remove dot only if it's preceeded by two lowercase letters
				regex := regexp.MustCompile(`[a-z]{2}\.$`)
				if regex.MatchString(trimmedName) {
					name = strings.TrimSuffix(trimmedName, ".")
				}
			}
			result[authority] = Corporate{
				Name:        name,
				Description: description,
				Role:        role,
				Identifier:  identifier,
				Level:       gndData.Level,
				Type:        gndData.Type,
				UseFor:      gndData.UseFor,
				Variant:     gndData.Variant,
				OtherId:     gndData.OtherId,
				Place:       gndData.Place,
			}
		}
		if strings.HasPrefix(linkedField, "880-") {
			linkVal := strings.TrimPrefix(linkedField, "880-")
			for _, field := range field880 {
				var name string
				var description []string
				var role []string
				var identifier string
				var authority string
				var found bool
				for _, subField := range field.Subfields {
					if subField.Code == "6" {
						if strings.HasPrefix(subField.Text, "110-") || strings.HasPrefix(subField.Text, "710-") {
							if subField.Text[4:6] == linkVal {
								found = true
							}
						}
					}
				}
				if !found {
					continue
				}
				for _, subField := range field.Subfields {
					switch subField.Code {
					case "a":
						name = subField.Text
					case "b":
						trimmedName := strings.Trim(name, " ")
						if strings.HasSuffix(trimmedName, ",") {
							name += " " + subField.Text
						} else {
							name += ", " + subField.Text
						}
					case "g":
						if source == "cz" {
							// remove suffix .,;
							cleanDescription := strings.TrimSuffix(subField.Text, ".")
							cleanDescription = strings.TrimSuffix(cleanDescription, ",")
							cleanDescription = strings.TrimSuffix(cleanDescription, ";")
							description = append(description, cleanDescription)
						} else {
							description = append(description, subField.Text)
						}
						description = append(description, subField.Text)
					case "0":
						if strings.HasPrefix(subField.Text, "(") {
							identifier = subField.Text
						}
					case "4":
						role = append(role, subField.Text)
					}
				}
				for prefix, a := range authorityPrefixes {
					if strings.HasPrefix(identifier, prefix) {
						authority = a
					}
				}
				if authority == "" {
					authority = "alternateRepresentation"
				}
				if authority == "gnd" && identifier != "" {
					var err error
					gndData, err = getCorporateGND(identifier, client)
					if err != nil {
						fmt.Printf("could not find gnd '%s': %v", identifier, err)
						// we only accept not found errors
						if !errors.Is(err, errorNotFound) {
							return nil, errors.Wrapf(err, "could not get corporate subject identifier '%s'", identifier)
						}
					}
				}

				if name != "" && gndData != nil {
					if authority != "gnd" || gndData == nil {
						gndData = &gndCorporateResult{}
					}
					trimmedName := strings.Trim(name, " ")
					if strings.HasSuffix(trimmedName, ",") {
						name = strings.TrimSuffix(trimmedName, ",")
					}
					if source == "cz" && strings.HasSuffix(trimmedName, ".") {
						// remove dot only if it's preceeded by two lowercase letters
						regex := regexp.MustCompile(`[a-z]{2}\.$`)
						if regex.MatchString(trimmedName) {
							name = strings.TrimSuffix(trimmedName, ".")
						}
					}
					result[authority] = Corporate{
						Name:        name,
						Description: description,
						Role:        role,
						Identifier:  identifier,
						Level:       gndData.Level,
						Type:        gndData.Type,
						UseFor:      gndData.UseFor,
						Variant:     gndData.Variant,
						OtherId:     gndData.OtherId,
						Place:       gndData.Place,
					}
				}
			}
		}
		if len(result) != 0 {
			allResult = append(allResult, result)
		}
	}
	if len(allResult) == 0 {
		return nil, nil
	}
	ar := []any{}
	for _, v := range allResult {
		ar = append(ar, v)
	}
	return ar, nil
}

func subjectCorporate(rec *marc21struct.QueryStruct, client *gnd) ([]any, error) {
	if rec == nil {
		return nil, errors.New("empty query struct")
	}
	var id string
	var source string
	for _, field := range rec.Datafields {
		if field.Tag == "035" {
			for _, sub := range field.Subfields {
				if sub.Code == "a" {
					id = sub.Text
					if strings.Contains(sub.Text, "EXLCZ") {
						source = "cz"
					}
				}
			}
		}
	}
	_ = id
	_ = source
	if rec.Datafields == nil {
		return nil, errors.New("no datafield in query struct")
	}
	var objects = map[string][]*marc21struct.Datafield{}
	for _, object := range rec.Datafields {
		if object == nil {
			return nil, errors.New("nil datafield in query struct")
		}

		if _, ok := objects[object.Tag]; !ok {
			objects[object.Tag] = []*marc21struct.Datafield{}
		}
		objects[object.Tag] = append(objects[object.Tag], object)
	}

	var allResult = []CorporateResult{}
	var subjectFields = []*marc21struct.Datafield{}
	field610, ok := objects["610"]
	if ok {
		for _, fld := range field610 {
			var doNotUse bool

			// 610  $$t works are no corporates
			for _, sub := range fld.Subfields {
				if sub.Code == "t" {
					doNotUse = true
					break
				}
			}
			if !doNotUse {
				subjectFields = append(subjectFields, fld)
			}
		}
	}
	for _, field := range subjectFields {
		var result = CorporateResult{}
		var name string
		var description []string
		var identifier string
		var authority string
		if len(result) != 0 {
			allResult = append(allResult, result)
		}
		for _, subField := range field.Subfields {
			switch subField.Code {
			case "a":
				name = subField.Text
			case "b":
				trimmedName := strings.Trim(name, " ")
				if strings.HasSuffix(trimmedName, ",") {
					name += " " + subField.Text
				} else {
					name += ", " + subField.Text
				}
			case "g":
				if source == "cz" {
					// remove suffix .,;
					cleanDescription := strings.TrimSuffix(subField.Text, ".")
					cleanDescription = strings.TrimSuffix(cleanDescription, ",")
					cleanDescription = strings.TrimSuffix(cleanDescription, ";")
					description = append(description, cleanDescription)
				} else {
					description = append(description, subField.Text)
				}
				description = append(description, subField.Text)
			case "0":
				if strings.HasPrefix(subField.Text, "(") {
					identifier = subField.Text
				}
			}
		}
		switch field.Ind2 {
		case "0":
			authority = "lcsh"
		case "2":
			authority = "mesh"
		case "7":
			for _, subfield := range field.Subfields {
				if subfield.Code == "2" {
					switch subfield.Text {
					case "gnd":
						authority = "gnd"
					case "idsbb":
						authority = "idsbb"
					case "idref":
						authority = "idref"
					}
				}
			}
		}
		if authority == "" {
			authority = "other"
		}
		var gndData = &gndCorporateResult{
			Level:   "",
			Type:    []string{},
			UseFor:  []string{},
			Variant: []string{},
			Related: []string{},
			OtherId: []string{},
			Place:   []string{},
		}
		if authority == "gnd" && identifier != "" {
			var err error
			gndData, err = getCorporateGND(identifier, client)
			if err != nil {

				fmt.Printf("could not find gnd '%s': %v", identifier, err)
				// we only accept not found errors
				if !errors.Is(err, errorNotFound) {
					return nil, errors.Wrapf(err, "could not get corporate subject identifier '%s'", identifier)
				}
			}
		}
		if name != "" {
			if authority != "gnd" || gndData == nil {
				gndData = &gndCorporateResult{
					Level:   "",
					Type:    []string{},
					UseFor:  []string{},
					Variant: []string{},
					Related: []string{},
					OtherId: []string{},
					Place:   []string{},
				}
			}
			trimmedName := strings.Trim(name, " ")
			if strings.HasSuffix(trimmedName, ",") {
				name = strings.TrimSuffix(trimmedName, ",")
			}
			if source == "cz" && strings.HasSuffix(trimmedName, ".") {
				// remove dot only if it's preceeded by two lowercase letters
				regex := regexp.MustCompile(`[a-z]{2}\.$`)
				if regex.MatchString(trimmedName) {
					name = strings.TrimSuffix(trimmedName, ".")
				}
			}
			result[authority] = Corporate{
				Name:        name,
				Description: description,
				Identifier:  identifier,
				Level:       gndData.Level,
				Type:        gndData.Type,
				UseFor:      gndData.UseFor,
				Variant:     gndData.Variant,
				Related:     gndData.Related,
				OtherId:     gndData.OtherId,
				Place:       gndData.Place,
			}
		}
		if len(result) != 0 {
			allResult = append(allResult, result)
		}
	}
	if len(allResult) == 0 {
		return nil, nil
	}
	ar := []any{}
	for _, v := range allResult {
		ar = append(ar, v)
	}
	return ar, nil
}
