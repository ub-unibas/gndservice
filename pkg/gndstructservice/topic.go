package gndstructservice

import (
	"emperror.dev/errors"
	"fmt"
	"gitlab.switch.ch/ub-unibas/gndservice/v2/pkg/marc21struct"
	"regexp"
	"strings"
)

type Topic struct {
	Label      string   `json:"label"`
	Identifier string   `json:"identifier,omitempty"`
	Level      string   `json:"level,omitempty"`
	Type       []string `json:"entityType,omitempty"`
	UseFor     []string `json:"useFor,omitempty"`
	Variant    []string `json:"variant,omitempty"`
	Mapped     []string `json:"mapped,omitempty"`
	OtherId    []string `json:"otherIdentifier,omitempty"`
}

type TopicResult map[string]Topic

func getTopicGND(identifier string, client *gnd) (*gndTopicResult, error) {
	rec, err := client.getGND(identifier)
	if err != nil {
		return nil, errors.Wrapf(err, "cannot get gnd '%s'", identifier)
	}
	gndData := parseGNDTopic(rec)
	return gndData, nil
}

func topic(rec *marc21struct.QueryStruct, client *gnd) ([]any, error) {
	if rec == nil {
		return nil, errors.New("empty query struct")
	}
	var source string
	for _, field := range rec.Datafields {
		if field.Tag == "035" {
			for _, sub := range field.Subfields {
				if sub.Code == "a" {
					if strings.Contains(sub.Text, "EXLCZ") {
						source = "cz"
					}
				}
			}
		}
	}
	_ = source
	if rec.Datafields == nil {
		return nil, errors.New("no datafield in query struct")
	}
	var objects = map[string][]*marc21struct.Datafield{}
	for _, object := range rec.Datafields {
		if object == nil {
			return nil, errors.New("nil datafield in query struct")
		}

		if _, ok := objects[object.Tag]; !ok {
			objects[object.Tag] = []*marc21struct.Datafield{}
		}
		objects[object.Tag] = append(objects[object.Tag], object)
	}

	var allResult = []TopicResult{}
	var subjectFields = []*marc21struct.Datafield{}
	field650, ok := objects["650"]
	if ok {
		subjectFields = append(subjectFields, field650...)
	}
	for _, field := range subjectFields {
		var result = TopicResult{}
		var label string
		var identifier string
		var authority string
		if len(result) != 0 {
			allResult = append(allResult, result)
		}
		for _, subField := range field.Subfields {
			switch subField.Code {
			case "a":
				label = subField.Text
			case "x":
				label += ", " + subField.Text
			case "g":
				label += ", " + subField.Text
			case "0":
				if strings.HasPrefix(subField.Text, "(") {
					identifier = subField.Text
				}
			}
		}
		switch field.Ind2 {
		case "0":
			authority = "lcsh"
		case "2":
			authority = "mesh"
		case "7":
			for _, subfield := range field.Subfields {
				if subfield.Code == "2" {
					switch subfield.Text {
					case "gnd":
						authority = "gnd"
					case "idsbb":
						authority = "idsbb"
					case "stw":
						authority = "stw"
					case "idref":
						authority = "idref"
					}
				}
			}
		}
		if authority == "" {
			authority = "other"
		}
		var gndData = &gndTopicResult{
			Level:   "",
			Type:    []string{},
			UseFor:  []string{},
			Variant: []string{},
			Mapped:  []string{},
			OtherId: []string{},
		}
		if authority == "gnd" && identifier != "" {
			var err error
			gndData, err = getTopicGND(identifier, client)
			if err != nil {
				fmt.Printf("could not find gnd '%s': %v", identifier, err)
				// we only accept not found errors
				if !errors.Is(err, errorNotFound) {
					return nil, errors.Wrapf(err, "could not get topic identifier '%s'", identifier)
				}
			}
		}
		if label != "" {
			if authority != "gnd" || gndData == nil {
				gndData = &gndTopicResult{
					Level:   "",
					Type:    []string{},
					UseFor:  []string{},
					Variant: []string{},
					Mapped:  []string{},
					OtherId: []string{},
				}
			}
			trimmedLabel := strings.Trim(label, " ")
			if source == "cz" && strings.HasSuffix(trimmedLabel, ".") {
				// remove dot only if it's preceeded by two lowercase letters
				regex := regexp.MustCompile(`[a-z]{2}\.$`)
				if regex.MatchString(trimmedLabel) {
					label = strings.TrimSuffix(trimmedLabel, ".")
				}
			}
			result[authority] = Topic{
				Label:      label,
				Identifier: identifier,
				Level:      gndData.Level,
				Type:       gndData.Type,
				UseFor:     gndData.UseFor,
				Variant:    gndData.Variant,
				Mapped:     gndData.Mapped,
				OtherId:    gndData.OtherId,
			}
		}
		if len(result) != 0 {
			allResult = append(allResult, result)
		}
	}
	if len(allResult) == 0 {
		return nil, nil
	}
	ar := []any{}
	for _, v := range allResult {
		ar = append(ar, v)
	}
	return ar, nil
}
