package gndstructservice

import (
	"emperror.dev/errors"
	"encoding/json"
	"gitlab.switch.ch/ub-unibas/gndservice/v2/pkg/marc21struct"
	"net/http"
	"net/url"
)

var errorNotFound = errors.New("not found")
var errorInternal = errors.New("internal error")
var errorBadRequest = errors.New("bad request")
var errorNoService = errors.New("no service")
var errorUnknown = errors.New("unknown error")
var errorServiceUnavailable = errors.New("service unavailable")

// NewGNDService returns a new GND service
func NewGNDService(httpClient *http.Client, baseURL *url.URL) *gnd {
	return &gnd{
		httpClient: httpClient,
		baseURL:    baseURL, // "https://localhost:12346/api/v1/",
	}
}

type gnd struct {
	httpClient *http.Client
	baseURL    *url.URL
}

func (g *gnd) getGND(identifier string) (*marc21struct.Record, error) {
	var urlStr = g.baseURL.JoinPath(identifier)

	// res, err := http.Get(urlStr)
	res, err := g.httpClient.Get(urlStr.String())
	if err != nil {
		return nil, errors.Wrapf(err, "could access gnd service for gnd '%s'", identifier)
	}
	defer res.Body.Close()
	if res.StatusCode == http.StatusNotFound {
		return nil, errors.Wrapf(errorNotFound, "could not find gnd '%s: %s'", identifier, res.Status)
	}
	if res.StatusCode == http.StatusInternalServerError {
		return nil, errors.Wrapf(errorInternal, "internal server error gnd '%s': %s", identifier, res.Status)
	}
	if res.StatusCode == http.StatusBadRequest {
		return nil, errors.Wrapf(errorBadRequest, "bad request gnd '%s': %s", identifier, res.Status)
	}
	if res.StatusCode != http.StatusOK {
		return nil, errors.Wrapf(errorUnknown, "unknown error gnd '%s'", identifier)
	}
	var rec = &marc21struct.Record{}
	dec := json.NewDecoder(res.Body)
	if err := dec.Decode(rec); err != nil {
		return nil, errors.Wrapf(err, "cannot decode result json of '%s'", identifier)
	}

	return rec, nil
}
