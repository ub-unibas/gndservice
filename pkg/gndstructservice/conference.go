package gndstructservice

import (
	"emperror.dev/errors"
	"fmt"
	"gitlab.switch.ch/ub-unibas/gndservice/v2/pkg/marc21struct"
	"strings"
)

type Conference struct {
	Name        string   `json:"namePart"`
	Date        string   `json:"date,omitempty"`
	Description []string `json:"description,omitempty"`
	Role        []string `json:"role,omitempty"`
	Identifier  string   `json:"identifier,omitempty"`
	Level       string   `json:"level,omitempty"`
	Type        []string `json:"entityType,omitempty"`
	UseFor      []string `json:"useFor,omitempty"`
	Variant     []string `json:"variant,omitempty"`
	OtherId     []string `json:"otherIdentifier,omitempty"`
}

type ConferenceResult map[string]Conference

func getConferenceGND(identifier string, gnd *gnd) (*gndConferenceResult, error) {
	rec, error := gnd.getGND(identifier)
	if error != nil {
		return nil, errors.Wrapf(error, "cannot get gnd '%s'", identifier)
	}
	gndData := parseGNDConference(rec)
	return gndData, nil
}

func conference(rec *marc21struct.QueryStruct, gnd *gnd) ([]any, error) {
	if rec == nil {
		return nil, errors.New("empty query struct")
	}
	var id string
	var source string
	for _, field := range rec.Datafields {
		if field.Tag == "035" {
			for _, sub := range field.Subfields {
				if sub.Code == "a" {
					id = sub.Text
					if strings.Contains(sub.Text, "EXLCZ") {
						source = "cz"
					}
				}
			}
		}
	}
	_ = id
	_ = source
	if rec.Datafields == nil {
		return nil, errors.New("no datafield in query struct")
	}
	var objects = map[string][]*marc21struct.Datafield{}
	for _, object := range rec.Datafields {
		if object == nil {
			return nil, errors.New("nil datafield in query struct")
		}

		if _, ok := objects[object.Tag]; !ok {
			objects[object.Tag] = []*marc21struct.Datafield{}
		}
		objects[object.Tag] = append(objects[object.Tag], object)
	}

	var allResult = []ConferenceResult{}
	var conferenceFields = []*marc21struct.Datafield{}
	field880, ok := objects["880"]
	if !ok {
		field880 = nil
	}
	field711, ok := objects["711"]
	if ok {
		for _, fld := range field711 {
			var doNotUse bool

			// 711  $$t works are no conferences
			for _, sub := range fld.Subfields {
				if sub.Code == "t" {
					doNotUse = true
					break
				}
			}
			if !doNotUse {
				conferenceFields = append(conferenceFields, fld)
			}
		}
	}
	field111, ok := objects["111"]
	if ok {
		conferenceFields = append(conferenceFields, field111...)
	}
	for _, field := range conferenceFields {
		var result = ConferenceResult{}
		var linkedField string
		var name string
		var date string
		var description []string
		var role []string
		var identifier string
		var authority string
		for _, subField := range field.Subfields {
			switch subField.Code {
			case "a":
				if source == "cz" {
					//remove suffix .,;
					cleanSub := strings.TrimSuffix(subField.Text, ".")
					cleanSub = strings.TrimSuffix(cleanSub, ",")
					cleanSub = strings.TrimSuffix(cleanSub, ";")
					name = cleanSub
				} else {
					name = subField.Text
				}
			case "n":
				if source == "cz" {
					// Remove parentheses at beginning and end, remove suffix .,;
					cleanSub := strings.TrimSuffix(subField.Text, ".")
					cleanSub = strings.TrimSuffix(cleanSub, ",")
					cleanSub = strings.TrimSuffix(cleanSub, ";")
					cleanSub = strings.TrimPrefix(cleanSub, "(")
					cleanSub = strings.TrimSuffix(cleanSub, ")")
					cleanSub = strings.TrimSuffix(cleanSub, ":")
					name += " " + strings.Trim(cleanSub, " ")
				} else {
					name += " " + subField.Text
				}
			case "c":
				if source == "cz" {
					// Remove parentheses at beginning and end, remove suffix .,;
					cleanSub := strings.TrimSuffix(subField.Text, ".")
					cleanSub = strings.TrimSuffix(cleanSub, ",")
					cleanSub = strings.TrimSuffix(cleanSub, ";")
					cleanSub = strings.TrimPrefix(cleanSub, "(")
					cleanSub = strings.TrimSuffix(cleanSub, ")")
					name += ", " + cleanSub
				} else {
					name += ", " + subField.Text
				}
			case "e":
				name += " " + subField.Text
			case "d":
				if source == "cz" {
					// Remove parentheses at beginning and end, remove suffix .,;
					cleanDate := strings.TrimSuffix(subField.Text, ".")
					cleanDate = strings.TrimSuffix(cleanDate, ",")
					cleanDate = strings.TrimSuffix(cleanDate, ";")
					cleanDate = strings.TrimPrefix(cleanDate, "(")
					cleanDate = strings.TrimSuffix(cleanDate, ")")
					cleanDate = strings.TrimSuffix(cleanDate, ":")
					date = strings.Trim(cleanDate, " ")
				} else {
					date = subField.Text
				}
			case "g":
				description = append(description, subField.Text)
			case "0":
				if strings.HasPrefix(subField.Text, "(") {
					identifier = subField.Text
				}
			case "4":
				role = append(role, subField.Text)
			case "6":
				linkedField = subField.Text
			}
		}
		for prefix, a := range authorityPrefixes {
			if strings.HasPrefix(identifier, prefix) {
				authority = a
			}
		}
		if authority == "" {
			authority = "unknown"
		}
		var gndData = &gndConferenceResult{
			Level:   "",
			Type:    []string{},
			UseFor:  []string{},
			Variant: []string{},
			OtherId: []string{},
		}
		if authority == "gnd" && identifier != "" {
			var err error
			gndData, err = getConferenceGND(identifier, gnd)
			if err != nil {
				fmt.Printf("could not find gnd '%s': %v", identifier, err)
				// we only accept not found errors
				if !errors.Is(err, errorNotFound) {
					return nil, errors.Wrapf(err, "could not get conference identifier '%s'", identifier)
				}
			}
		}
		if name != "" {
			if authority != "gnd" || gndData == nil {
				gndData = &gndConferenceResult{
					Level:   "",
					Type:    []string{},
					UseFor:  []string{},
					Variant: []string{},
					OtherId: []string{},
				}
			}
			result[authority] = Conference{
				Name:        name,
				Date:        date,
				Description: description,
				Role:        role,
				Identifier:  identifier,
				Level:       gndData.Level,
				Type:        gndData.Type,
				UseFor:      gndData.UseFor,
				Variant:     gndData.Variant,
				OtherId:     gndData.OtherId,
			}
		}
		if strings.HasPrefix(linkedField, "880-") {
			linkVal := strings.TrimPrefix(linkedField, "880-")
			for _, field := range field880 {
				var name string
				var date string
				var description []string
				var role []string
				var identifier string
				var authority string
				var found bool
				for _, subField := range field.Subfields {
					if subField.Code == "6" {
						if strings.HasPrefix(subField.Text, "111-") || strings.HasPrefix(subField.Text, "711-") {
							if subField.Text[4:6] == linkVal {
								found = true
							}
						}
					}
				}
				if !found {
					continue
				}
				for _, subField := range field.Subfields {
					switch subField.Code {
					case "a":
						if source == "cz" {
							//remove suffix .,;
							cleanSub := strings.TrimSuffix(subField.Text, ".")
							cleanSub = strings.TrimSuffix(cleanSub, ",")
							cleanSub = strings.TrimSuffix(cleanSub, ";")
							name = cleanSub
						} else {
							name = subField.Text
						}
					case "n":
						if source == "cz" {
							// Remove parentheses at beginning and end, remove suffix .,;
							cleanSub := strings.TrimSuffix(subField.Text, ".")
							cleanSub = strings.TrimSuffix(cleanSub, ",")
							cleanSub = strings.TrimSuffix(cleanSub, ";")
							cleanSub = strings.TrimPrefix(cleanSub, "(")
							cleanSub = strings.TrimSuffix(cleanSub, ")")
							name += " " + cleanSub
						} else {
							name += " " + subField.Text
						}
					case "c":
						if source == "cz" {
							// Remove parentheses at beginning and end, remove suffix .,;
							cleanSub := strings.TrimSuffix(subField.Text, ".")
							cleanSub = strings.TrimSuffix(cleanSub, ",")
							cleanSub = strings.TrimSuffix(cleanSub, ";")
							cleanSub = strings.TrimPrefix(cleanSub, "(")
							cleanSub = strings.TrimSuffix(cleanSub, ")")
							name += ", " + cleanSub
						} else {
							name += ", " + subField.Text
						}
					case "e":
						name += " " + subField.Text
					case "d":
						if source == "cz" {
							// Remove parentheses at beginning and end, remove suffix .,;
							cleanDate := strings.TrimSuffix(subField.Text, ".")
							cleanDate = strings.TrimSuffix(cleanDate, ",")
							cleanDate = strings.TrimSuffix(cleanDate, ";")
							cleanDate = strings.TrimPrefix(cleanDate, "(")
							cleanDate = strings.TrimSuffix(cleanDate, ")")
							date = cleanDate
						} else {
							date = subField.Text
						}
					case "g":
						description = append(description, subField.Text)
					case "0":
						if strings.HasPrefix(subField.Text, "(") {
							identifier = subField.Text
						}
					case "4":
						role = append(role, subField.Text)
					}
				}
				for prefix, a := range authorityPrefixes {
					if strings.HasPrefix(identifier, prefix) {
						authority = a
					}
				}
				if authority == "" {
					authority = "alternateRepresentation"
				}
				if authority == "gnd" && identifier != "" {
					var err error
					gndData, err = getConferenceGND(identifier, gnd)
					if err != nil {
						fmt.Printf("could not find gnd '%s': %v", identifier, err)
						// we only accept not found errors
						if !errors.Is(err, errorNotFound) {
							return nil, errors.Wrapf(err, "could not get conference identifier '%s'", identifier)
						}
					}
				}

				if name != "" && gndData != nil {
					if authority != "gnd" || gndData == nil {
						gndData = &gndConferenceResult{}
					}
					result[authority] = Conference{
						Name:        name,
						Date:        date,
						Description: description,
						Role:        role,
						Identifier:  identifier,
						Level:       gndData.Level,
						Type:        gndData.Type,
						UseFor:      gndData.UseFor,
						Variant:     gndData.Variant,
						OtherId:     gndData.OtherId,
					}
				}
			}
		}
		if len(result) != 0 {
			allResult = append(allResult, result)
		}
	}
	if len(allResult) == 0 {
		return nil, nil
	}
	ar := []any{}
	for _, v := range allResult {
		ar = append(ar, v)
	}
	return ar, nil
}

func subjectConference(rec *marc21struct.QueryStruct, gnd *gnd) ([]any, error) {
	if rec == nil {
		return nil, errors.New("empty query struct")
	}
	var id string
	var source string
	for _, field := range rec.Datafields {
		if field.Tag == "035" {
			for _, sub := range field.Subfields {
				if sub.Code == "a" {
					id = sub.Text
					if strings.Contains(sub.Text, "EXLCZ") {
						source = "cz"
					}
				}
			}
		}
	}
	_ = id
	_ = source
	if rec.Datafields == nil {
		return nil, errors.New("no datafield in query struct")
	}
	var objects = map[string][]*marc21struct.Datafield{}
	for _, object := range rec.Datafields {
		if object == nil {
			return nil, errors.New("nil datafield in query struct")
		}

		if _, ok := objects[object.Tag]; !ok {
			objects[object.Tag] = []*marc21struct.Datafield{}
		}
		objects[object.Tag] = append(objects[object.Tag], object)
	}

	var allResult = []ConferenceResult{}
	var subjectFields = []*marc21struct.Datafield{}
	field611, ok := objects["611"]
	if ok {
		for _, fld := range field611 {
			var doNotUse bool

			// 611  $$t works are no conferences
			for _, sub := range fld.Subfields {
				if sub.Code == "t" {
					doNotUse = true
					break
				}
			}
			if !doNotUse {
				subjectFields = append(subjectFields, fld)
			}
		}
	}
	for _, field := range subjectFields {
		var result = ConferenceResult{}
		var name string
		var date string
		var description []string
		var identifier string
		var authority string
		if len(result) != 0 {
			allResult = append(allResult, result)
		}
		for _, subField := range field.Subfields {
			switch subField.Code {
			case "a":
				if source == "cz" {
					//remove suffix .,;
					cleanSub := strings.TrimSuffix(subField.Text, ".")
					cleanSub = strings.TrimSuffix(cleanSub, ",")
					cleanSub = strings.TrimSuffix(cleanSub, ";")
					name = cleanSub
				} else {
					name = subField.Text
				}
			case "n":
				if source == "cz" {
					// Remove parentheses at beginning and end, remove suffix .,;
					cleanSub := strings.TrimSuffix(subField.Text, ".")
					cleanSub = strings.TrimSuffix(cleanSub, ",")
					cleanSub = strings.TrimSuffix(cleanSub, ";")
					cleanSub = strings.TrimPrefix(cleanSub, "(")
					cleanSub = strings.TrimSuffix(cleanSub, ")")
					name += " " + cleanSub
				} else {
					name += " " + subField.Text
				}
			case "c":
				if source == "cz" {
					// Remove parentheses at beginning and end, remove suffix .,;
					cleanSub := strings.TrimSuffix(subField.Text, ".")
					cleanSub = strings.TrimSuffix(cleanSub, ",")
					cleanSub = strings.TrimSuffix(cleanSub, ";")
					cleanSub = strings.TrimPrefix(cleanSub, "(")
					cleanSub = strings.TrimSuffix(cleanSub, ")")
					name += ", " + cleanSub
				} else {
					name += ", " + subField.Text
				}
			case "e":
				name += " " + subField.Text
			case "d":
				if source == "cz" {
					// Remove parentheses at beginning and end, remove suffix .,;
					cleanDate := strings.TrimSuffix(subField.Text, ".")
					cleanDate = strings.TrimSuffix(cleanDate, ",")
					cleanDate = strings.TrimSuffix(cleanDate, ";")
					cleanDate = strings.TrimPrefix(cleanDate, "(")
					cleanDate = strings.TrimSuffix(cleanDate, ")")
					date = cleanDate
				} else {
					date = subField.Text
				}
			case "g":
				description = append(description, subField.Text)
			case "0":
				if strings.HasPrefix(subField.Text, "(") {
					identifier = subField.Text
				}
			}
		}
		switch field.Ind2 {
		case "0":
			authority = "lcsh"
		case "2":
			authority = "mesh"
		case "7":
			for _, subfield := range field.Subfields {
				if subfield.Code == "2" {
					switch subfield.Text {
					case "gnd":
						authority = "gnd"
					case "idsbb":
						authority = "idsbb"
					case "idref":
						authority = "idref"
					}
				}
			}
		}
		if authority == "" {
			authority = "other"
		}
		var gndData = &gndConferenceResult{
			Level:   "",
			Type:    []string{},
			UseFor:  []string{},
			Variant: []string{},
			OtherId: []string{},
		}
		if authority == "gnd" && identifier != "" {
			var err error
			gndData, err = getConferenceGND(identifier, gnd)
			if err != nil {
				fmt.Printf("could not find gnd '%s': %v", identifier, err)
				// we only accept not found errors
				if !errors.Is(err, errorNotFound) {
					return nil, errors.Wrapf(err, "could not get conference identifier '%s'", identifier)
				}
			}
		}
		if name != "" {
			if authority != "gnd" || gndData == nil {
				gndData = &gndConferenceResult{
					Level:   "",
					Type:    []string{},
					UseFor:  []string{},
					Variant: []string{},
					OtherId: []string{},
				}
			}
			result[authority] = Conference{
				Name:        name,
				Date:        date,
				Description: description,
				Identifier:  identifier,
				Level:       gndData.Level,
				Type:        gndData.Type,
				UseFor:      gndData.UseFor,
				Variant:     gndData.Variant,
				OtherId:     gndData.OtherId,
			}
		}
		if len(result) != 0 {
			allResult = append(allResult, result)
		}
	}
	if len(allResult) == 0 {
		return nil, nil
	}
	ar := []any{}
	for _, v := range allResult {
		ar = append(ar, v)
	}
	return ar, nil
}
