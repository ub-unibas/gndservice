package gndstructservice

import (
	"emperror.dev/errors"
	"fmt"
	"gitlab.switch.ch/ub-unibas/gndservice/v2/pkg/marc21struct"
	"regexp"
	"strings"
)

type Title struct {
	Name       string   `json:"name,omitempty"`
	Identifier string   `json:"identifier,omitempty"`
	Title      string   `json:"title"`
	Level      string   `json:"level,omitempty"`
	Type       []string `json:"entityType,omitempty"`
	UseFor     []string `json:"useFor,omitempty"`
	Variant    []string `json:"variant,omitempty"`
	OtherId    []string `json:"otherIdentifier,omitempty"`
}

type TitleResult map[string]Title

func getTitleGND(identifier string, client *gnd) (*gndTitleResult, error) {
	rec, err := client.getGND(identifier)
	if err != nil {
		return nil, errors.Wrapf(err, "cannot get gnd '%s'", identifier)
	}
	gndData := parseGNDTitle(rec)
	return gndData, nil
}

func title(rec *marc21struct.QueryStruct, client *gnd) ([]any, error) {
	if rec == nil {
		return nil, errors.New("empty query struct")
	}
	var source string
	for _, field := range rec.Datafields {
		if field.Tag == "035" {
			for _, sub := range field.Subfields {
				if sub.Code == "a" {
					if strings.Contains(sub.Text, "EXLCZ") {
						source = "cz"
					}
				}
			}
		}
	}
	if rec.Datafields == nil {
		return nil, errors.New("no datafield in query struct")
	}
	var objects = map[string][]*marc21struct.Datafield{}
	for _, object := range rec.Datafields {
		if object == nil {
			return nil, errors.New("nil datafield in query struct")
		}

		if _, ok := objects[object.Tag]; !ok {
			objects[object.Tag] = []*marc21struct.Datafield{}
		}
		objects[object.Tag] = append(objects[object.Tag], object)
	}

	var allResult = []TitleResult{}
	var titleFields = []*marc21struct.Datafield{}
	field880, ok := objects["880"]
	if !ok {
		field880 = nil
	}
	field700, ok := objects["700"]
	if ok {
		titleFields = append(titleFields, field700...)
	}
	field710, ok := objects["710"]
	if ok {
		titleFields = append(titleFields, field710...)
	}
	field711, ok := objects["711"]
	if ok {
		titleFields = append(titleFields, field711...)
	}
	field130, ok := objects["130"]
	if ok {
		titleFields = append(titleFields, field130...)
	}
	field730, ok := objects["730"]
	if ok {
		titleFields = append(titleFields, field730...)
	}
	for _, field := range titleFields {
		var result = TitleResult{}
		var linkedField string
		var name string
		var title string
		var identifier string
		var authority string
		var ok bool
		for _, subField := range field.Subfields {
			if field.Tag == "130" {
				ok = true
				break
			}
			if field.Tag == "700" && subField.Code == "t" {
				ok = true
				break
			}
			if field.Tag == "710" && subField.Code == "t" {
				ok = true
				break
			}
			if field.Tag == "711" && subField.Code == "t" {
				ok = true
				break
			}
			if field.Tag == "730" {
				ok = true
				break
			}
		}

		if ok {
			for _, subField := range field.Subfields {
				switch field.Tag {
				case "130", "730":
					switch subField.Code {
					case "a":
						title = subField.Text
					case "g":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "m":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ",") {
							title += " " + subField.Text
						} else {
							title += ", " + subField.Text
						}
					case "n":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "r":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ",") {
							title += " " + subField.Text
						} else {
							title += ", " + subField.Text
						}
					case "f":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "s":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "p":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ",") {
							title += " " + subField.Text
						} else {
							title += ", " + subField.Text
						}
					case "k":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "o":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ";") {
							title += " " + subField.Text
						} else {
							title += " ; " + subField.Text
						}
					case "0":
						if strings.HasPrefix(subField.Text, "(") {
							identifier = subField.Text
						}
					}
				case "700":
					switch subField.Code {
					case "t":
						title = subField.Text
					case "g":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "m":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ",") {
							title += " " + subField.Text
						} else {
							title += ", " + subField.Text
						}
					case "n":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "r":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ",") {
							title += " " + subField.Text
						} else {
							title += ", " + subField.Text
						}
					case "f":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "s":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "p":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ",") {
							title += " " + subField.Text
						} else {
							title += ", " + subField.Text
						}
					case "k":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "o":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ";") {
							title += " " + subField.Text
						} else {
							title += " ; " + subField.Text
						}
					case "0":
						if strings.HasPrefix(subField.Text, "(") {
							identifier = subField.Text
						}
					case "a":
						name = subField.Text
					case "b":
						name += " " + subField.Text
					case "c":
						trimmedName := strings.Trim(name, " ")
						if strings.HasSuffix(trimmedName, ",") {
							name += " " + subField.Text
						} else {
							name += ", " + subField.Text
						}
					case "d":
						if source == "cz" {
							// Remove parentheses at beginning and end, remove suffix .,;
							cleanDate := strings.TrimSuffix(subField.Text, ".")
							cleanDate = strings.TrimSuffix(cleanDate, ",")
							cleanDate = strings.TrimSuffix(cleanDate, ";")
							cleanDate = strings.TrimPrefix(cleanDate, "(")
							cleanDate = strings.TrimSuffix(cleanDate, ")")
							name += " (" + subField.Text + ")"
						} else {
							name += " (" + subField.Text + ")"
						}
					}
				case "710":
					switch subField.Code {
					case "t":
						title = subField.Text
					case "g":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "m":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ",") {
							title += " " + subField.Text
						} else {
							title += ", " + subField.Text
						}
					case "n":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "r":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ",") {
							title += " " + subField.Text
						} else {
							title += ", " + subField.Text
						}
					case "f":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "s":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "p":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ",") {
							title += " " + subField.Text
						} else {
							title += ", " + subField.Text
						}
					case "k":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "o":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ";") {
							title += " " + subField.Text
						} else {
							title += " ; " + subField.Text
						}
					case "0":
						if strings.HasPrefix(subField.Text, "(") {
							identifier = subField.Text
						}
					case "a":
						name = subField.Text
					case "b":
						trimmedName := strings.Trim(name, " ")
						if strings.HasSuffix(trimmedName, ",") {
							name += " " + subField.Text
						} else {
							name += ", " + subField.Text
						}
					}
				case "711":
					switch subField.Code {
					case "t":
						title = subField.Text
					case "g":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "m":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ",") {
							title += " " + subField.Text
						} else {
							title += ", " + subField.Text
						}
					case "n":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "r":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ",") {
							title += " " + subField.Text
						} else {
							title += ", " + subField.Text
						}
					case "f":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "s":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "p":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ",") {
							title += " " + subField.Text
						} else {
							title += ", " + subField.Text
						}
					case "k":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "o":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ";") {
							title += " " + subField.Text
						} else {
							title += " ; " + subField.Text
						}
					case "0":
						if strings.HasPrefix(subField.Text, "(") {
							identifier = subField.Text
						}
					case "a":
						name = subField.Text
					case "c":
						name += ", " + subField.Text
					case "e":
						name += " " + subField.Text
					}
				}
			}
		}
		for prefix, a := range authorityPrefixes {
			if strings.HasPrefix(identifier, prefix) {
				authority = a
			}
		}
		if authority == "" {
			authority = "unknown"
		}
		var gndData = &gndTitleResult{
			Level:   "",
			Type:    []string{},
			UseFor:  []string{},
			Variant: []string{},
			OtherId: []string{},
		}
		if authority == "gnd" && identifier != "" {
			var err error
			gndData, err = getTitleGND(identifier, client)
			if err != nil {
				fmt.Printf("could not find gnd '%s': %v", identifier, err)
				// we only accept not found errors
				if !errors.Is(err, errorNotFound) {
					return nil, errors.Wrapf(err, "could not get title identifier '%s'", identifier)
				}
			}
		}
		if title != "" {
			if authority != "gnd" || gndData == nil {
				gndData = &gndTitleResult{
					Level:   "",
					Type:    []string{},
					UseFor:  []string{},
					Variant: []string{},
					OtherId: []string{},
				}
			}
			trimmedName := strings.Trim(name, " ")
			if source == "cz" && strings.HasSuffix(trimmedName, ".") {
				// remove dot only if it's preceeded by two lowercase letters
				regex := regexp.MustCompile(`[a-z]{2}\.$`)
				if regex.MatchString(trimmedName) {
					name = strings.TrimSuffix(trimmedName, ".")
				}
			}
			result[authority] = Title{
				Name:       name,
				Title:      title,
				Identifier: identifier,
				Level:      gndData.Level,
				Type:       gndData.Type,
				UseFor:     gndData.UseFor,
				Variant:    gndData.Variant,
				OtherId:    gndData.OtherId,
			}
		}
		if strings.HasPrefix(linkedField, "880-") {
			linkVal := strings.TrimPrefix(linkedField, "880-")
			for _, field := range field880 {
				var name string
				var title string
				var identifier string
				var authority string
				var found bool
				for _, subField := range field.Subfields {
					if subField.Code == "6" {
						if strings.HasPrefix(subField.Text, "130-") || strings.HasPrefix(subField.Text, "700-") || strings.HasPrefix(subField.Text, "710-") || strings.HasPrefix(subField.Text, "730-") {
							if subField.Text[4:6] == linkVal {
								found = true
							}
						}
					}
				}
				if !found {
					continue
				}
				for _, subField := range field.Subfields {
					if subField.Code == "6" {
						if strings.HasPrefix(subField.Text, "130-") || strings.HasPrefix(subField.Text, "730-") {
							switch subField.Code {
							case "a":
								title = subField.Text
							case "g":
								title += ". " + subField.Text
							case "m":
								title += ", " + subField.Text
							case "n":
								title += ". " + subField.Text
							case "r":
								title += ", " + subField.Text
							case "f":
								title += ". " + subField.Text
							case "s":
								title += ". " + subField.Text
							case "p":
								title += ", " + subField.Text
							case "k":
								title += ". " + subField.Text
							case "o":
								title += " ; " + subField.Text
							case "0":
								if strings.HasPrefix(subField.Text, "(") {
									identifier = subField.Text
								}
							}
						}
						if strings.HasPrefix(subField.Text, "700-") {
							switch subField.Code {
							case "t":
								title = subField.Text
							case "g":
								title += ". " + subField.Text
							case "m":
								title += ", " + subField.Text
							case "n":
								title += ". " + subField.Text
							case "r":
								title += ", " + subField.Text
							case "f":
								title += ". " + subField.Text
							case "s":
								title += ". " + subField.Text
							case "p":
								title += ", " + subField.Text
							case "k":
								title += ". " + subField.Text
							case "o":
								title += " ; " + subField.Text
							case "0":
								if strings.HasPrefix(subField.Text, "(") {
									identifier = subField.Text
								}
							case "a":
								name = subField.Text
							case "b":
								name += " " + subField.Text
							case "c":
								name += ", " + subField.Text
							case "d":
								name += " (" + subField.Text + ")"
							}
						}
						if strings.HasPrefix(subField.Text, "710-") {
							switch subField.Code {
							case "t":
								title = subField.Text
							case "g":
								title += ". " + subField.Text
							case "m":
								title += ", " + subField.Text
							case "n":
								title += ". " + subField.Text
							case "r":
								title += ", " + subField.Text
							case "f":
								title += ". " + subField.Text
							case "s":
								title += ". " + subField.Text
							case "p":
								title += ", " + subField.Text
							case "k":
								title += ". " + subField.Text
							case "o":
								title += " ; " + subField.Text
							case "0":
								if strings.HasPrefix(subField.Text, "(") {
									identifier = subField.Text
								}
							case "a":
								name = subField.Text
							case "b":
								name += ", " + subField.Text
							}
						}
						if strings.HasPrefix(subField.Text, "711-") {
							switch subField.Code {
							case "t":
								title = subField.Text
							case "g":
								title += ". " + subField.Text
							case "m":
								title += ", " + subField.Text
							case "n":
								title += ". " + subField.Text
							case "r":
								title += ", " + subField.Text
							case "f":
								title += ". " + subField.Text
							case "s":
								title += ". " + subField.Text
							case "p":
								title += ", " + subField.Text
							case "k":
								title += ". " + subField.Text
							case "o":
								title += " ; " + subField.Text
							case "0":
								if strings.HasPrefix(subField.Text, "(") {
									identifier = subField.Text
								}
							case "a":
								name = subField.Text
							case "c":
								name += ", " + subField.Text
							case "e":
								name += " " + subField.Text
							}
						}
					}
				}
				for prefix, a := range authorityPrefixes {
					if strings.HasPrefix(identifier, prefix) {
						authority = a
					}
				}
				if authority == "" {
					authority = "alternateRepresentation"
				}
				if authority == "gnd" && identifier != "" {
					var err error
					gndData, err = getTitleGND(identifier, client)
					if err != nil {
						fmt.Printf("could not find gnd '%s': %v", identifier, err)
						// we only accept not found errors
						if !errors.Is(err, errorNotFound) {
							return nil, errors.Wrapf(err, "could not get title identifier '%s'", identifier)
						}
					}
				}

				if title != "" && gndData != nil {
					if authority != "gnd" || gndData == nil {
						gndData = &gndTitleResult{}
					}
					result[authority] = Title{
						Name:       name,
						Title:      title,
						Identifier: identifier,
						Level:      gndData.Level,
						Type:       gndData.Type,
						UseFor:     gndData.UseFor,
						Variant:    gndData.Variant,
						OtherId:    gndData.OtherId,
					}
				}
			}
		}
		if len(result) != 0 {
			allResult = append(allResult, result)
		}
	}
	if len(allResult) == 0 {
		return nil, nil
	}
	ar := []any{}
	for _, v := range allResult {
		ar = append(ar, v)
	}
	return ar, nil
}

func subjectTitle(rec *marc21struct.QueryStruct, client *gnd) ([]any, error) {
	if rec == nil {
		return nil, errors.New("empty query struct")
	}
	var source string
	for _, field := range rec.Datafields {
		if field.Tag == "035" {
			for _, sub := range field.Subfields {
				if sub.Code == "a" {
					if strings.Contains(sub.Text, "EXLCZ") {
						source = "cz"
					}
				}
			}
		}
	}
	if rec.Datafields == nil {
		return nil, errors.New("no datafield in query struct")
	}
	var objects = map[string][]*marc21struct.Datafield{}
	for _, object := range rec.Datafields {
		if object == nil {
			return nil, errors.New("nil datafield in query struct")
		}

		if _, ok := objects[object.Tag]; !ok {
			objects[object.Tag] = []*marc21struct.Datafield{}
		}
		objects[object.Tag] = append(objects[object.Tag], object)
	}

	var allResult = []TitleResult{}
	var subjectFields = []*marc21struct.Datafield{}
	field600, ok := objects["600"]
	if ok {
		subjectFields = append(subjectFields, field600...)
	}
	field610, ok := objects["610"]
	if ok {
		subjectFields = append(subjectFields, field610...)
	}
	field611, ok := objects["611"]
	if ok {
		subjectFields = append(subjectFields, field611...)
	}
	field630, ok := objects["630"]
	if ok {
		subjectFields = append(subjectFields, field630...)
	}
	for _, field := range subjectFields {
		var result = TitleResult{}
		var name string
		var title string
		var identifier string
		var authority string
		if len(result) != 0 {
			allResult = append(allResult, result)
		}
		var ok bool
		for _, subField := range field.Subfields {
			if field.Tag == "600" && subField.Code == "t" {
				ok = true
				break
			}
			if field.Tag == "610" && subField.Code == "t" {
				ok = true
				break
			}
			if field.Tag == "611" && subField.Code == "t" {
				ok = true
				break
			}
			if field.Tag == "630" {
				ok = true
				break
			}
		}
		if ok {
			for _, subField := range field.Subfields {
				switch field.Tag {
				case "630":
					switch subField.Code {
					case "a":
						title = subField.Text
					case "g":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "m":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ",") {
							title += " " + subField.Text
						} else {
							title += ", " + subField.Text
						}
					case "n":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "r":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ",") {
							title += " " + subField.Text
						} else {
							title += ", " + subField.Text
						}
					case "f":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "s":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "p":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ",") {
							title += " " + subField.Text
						} else {
							title += ", " + subField.Text
						}
					case "k":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "o":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ";") {
							title += " " + subField.Text
						} else {
							title += " ; " + subField.Text
						}
					case "0":
						if strings.HasPrefix(subField.Text, "(") {
							identifier = subField.Text
						}
					}
				case "600":
					switch subField.Code {
					case "t":
						title = subField.Text
					case "g":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "m":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ",") {
							title += " " + subField.Text
						} else {
							title += ", " + subField.Text
						}
					case "n":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "r":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ",") {
							title += " " + subField.Text
						} else {
							title += ", " + subField.Text
						}
					case "f":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "s":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "p":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ",") {
							title += " " + subField.Text
						} else {
							title += ", " + subField.Text
						}
					case "k":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "o":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ";") {
							title += " " + subField.Text
						} else {
							title += " ; " + subField.Text
						}
					case "0":
						if strings.HasPrefix(subField.Text, "(") {
							identifier = subField.Text
						}
					case "a":
						name = subField.Text
					case "b":
						name += " " + subField.Text
					case "c":
						trimmedName := strings.Trim(name, " ")
						if strings.HasSuffix(trimmedName, ",") {
							name += " " + subField.Text
						} else {
							name += ", " + subField.Text
						}
					case "d":
						if source == "cz" {
							// Remove parentheses at beginning and end, remove suffix .,;
							cleanDate := strings.TrimSuffix(subField.Text, ".")
							cleanDate = strings.TrimSuffix(cleanDate, ",")
							cleanDate = strings.TrimSuffix(cleanDate, ";")
							cleanDate = strings.TrimPrefix(cleanDate, "(")
							cleanDate = strings.TrimSuffix(cleanDate, ")")
							name += " (" + subField.Text + ")"
						} else {
							name += " (" + subField.Text + ")"
						}
					}
				case "610":
					switch subField.Code {
					case "t":
						title = subField.Text
					case "g":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "m":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ",") {
							title += " " + subField.Text
						} else {
							title += ", " + subField.Text
						}
					case "n":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "r":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ",") {
							title += " " + subField.Text
						} else {
							title += ", " + subField.Text
						}
					case "f":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "s":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "p":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ",") {
							title += " " + subField.Text
						} else {
							title += ", " + subField.Text
						}
					case "k":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "o":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ";") {
							title += " " + subField.Text
						} else {
							title += " ; " + subField.Text
						}
					case "0":
						if strings.HasPrefix(subField.Text, "(") {
							identifier = subField.Text
						}
					case "a":
						name = subField.Text
					case "b":
						trimmedName := strings.Trim(name, " ")
						if strings.HasSuffix(trimmedName, ",") {
							name += " " + subField.Text
						} else {
							name += ", " + subField.Text
						}
					}
				case "611":
					switch subField.Code {
					case "t":
						title = subField.Text
					case "g":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "m":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ",") {
							title += " " + subField.Text
						} else {
							title += ", " + subField.Text
						}
					case "n":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "r":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ",") {
							title += " " + subField.Text
						} else {
							title += ", " + subField.Text
						}
					case "f":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "s":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "p":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ",") {
							title += " " + subField.Text
						} else {
							title += ", " + subField.Text
						}
					case "k":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ".") {
							title += " " + subField.Text
						} else {
							title += ". " + subField.Text
						}
					case "o":
						trimmedTitle := strings.Trim(title, " ")
						if strings.HasSuffix(trimmedTitle, ";") {
							title += " " + subField.Text
						} else {
							title += " ; " + subField.Text
						}
					case "0":
						if strings.HasPrefix(subField.Text, "(") {
							identifier = subField.Text
						}
					case "a":
						name = subField.Text
					case "c":
						name += ", " + subField.Text
					case "e":
						name += " " + subField.Text
					}
				}
				switch field.Ind2 {
				case "0":
					authority = "lcsh"
				case "2":
					authority = "mesh"
				case "7":
					for _, subfield := range field.Subfields {
						if subfield.Code == "2" {
							switch subfield.Text {
							case "gnd":
								authority = "gnd"
							case "idsbb":
								authority = "idsbb"
							case "idref":
								authority = "idref"
							}
						}
					}
				}
			}
		}
		if authority == "" {
			authority = "other"
		}
		var gndData = &gndTitleResult{
			Level:   "",
			Type:    []string{},
			UseFor:  []string{},
			Variant: []string{},
			OtherId: []string{},
		}
		if authority == "gnd" && identifier != "" {
			var err error
			gndData, err = getTitleGND(identifier, client)
			if err != nil {
				fmt.Printf("could not find gnd '%s': %v", identifier, err)
				// we only accept not found errors
				if !errors.Is(err, errorNotFound) {
					return nil, errors.Wrapf(err, "could not get conference identifier '%s'", identifier)
				}
			}
		}
		if title != "" {
			if authority != "gnd" || gndData == nil {
				gndData = &gndTitleResult{
					Level:   "",
					Type:    []string{},
					UseFor:  []string{},
					Variant: []string{},
					OtherId: []string{},
				}
			}
			trimmedName := strings.Trim(name, " ")
			if source == "cz" && strings.HasSuffix(trimmedName, ".") {
				// remove dot only if it's preceeded by two lowercase letters
				regex := regexp.MustCompile(`[a-z]{2}\.$`)
				if regex.MatchString(trimmedName) {
					name = strings.TrimSuffix(trimmedName, ".")
				}
			}
			result[authority] = Title{
				Name:       name,
				Title:      title,
				Identifier: identifier,
				Level:      gndData.Level,
				Type:       gndData.Type,
				UseFor:     gndData.UseFor,
				Variant:    gndData.Variant,
				OtherId:    gndData.OtherId,
			}
		}
		if len(result) != 0 {
			allResult = append(allResult, result)
		}
	}
	if len(allResult) == 0 {
		return nil, nil
	}
	ar := []any{}
	for _, v := range allResult {
		ar = append(ar, v)
	}
	return ar, nil
}
