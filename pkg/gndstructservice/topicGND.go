package gndstructservice

import "gitlab.switch.ch/ub-unibas/gndservice/v2/pkg/marc21struct"

type gndTopicResult struct {
	Level   string   `json:"level,omitempty"`
	Type    []string `json:"entityType,omitempty"`
	UseFor  []string `json:"usefor,omitempty"`
	Variant []string `json:"variant,omitempty"`
	Mapped  []string `json:"mapped,omitempty"`
	OtherId []string `json:"otherIdentifier,omitempty"`
}

func parseGNDTopic(rec *marc21struct.Record) *gndTopicResult {
	var result = &gndTopicResult{
		Level:   "",
		Type:    []string{},
		UseFor:  []string{},
		Variant: []string{},
		Mapped:  []string{},
		OtherId: []string{},
	}
	for _, df := range rec.Datafields {
		switch df.Tag {
		case "024":
			source := getSubfield(df.Subfields, "2")
			id := getSubfield(df.Subfields, "a")
			if len(source) == 1 && len(id) == 1 {
				for index, s := range source {
					if s != "gnd" {
						result.OtherId = append(result.OtherId, "("+s+")"+id[index])
					}
				}
			}
		case "042":
			levels := getSubfield(df.Subfields, "a")
			if len(levels) > 0 {
				result.Level = levels[0]
			}
		case "075":
			result.Type = append(result.Type, getSubfield(df.Subfields, "b")...)
		case "079":
			result.UseFor = append(result.UseFor, getSubfield(df.Subfields, "q")...)
		case "450":
			result.Variant = append(result.Variant, getSubfield(df.Subfields, "a")...)
		case "750":
			result.Mapped = append(result.Mapped, getSubfield(df.Subfields, "a")...)
		case "260":
			result.Type = append(result.Type, "Hinweissatz")
		}
	}
	if len(result.Variant) == 0 {
		result.Variant = nil
	}
	if len(result.Mapped) == 0 {
		result.Mapped = nil
	}
	if len(result.UseFor) == 0 {
		result.UseFor = nil
	}
	if len(result.Type) == 0 {
		result.Type = nil
	}

	return result
}
