package gndstructservice

import "gitlab.switch.ch/ub-unibas/gndservice/v2/pkg/marc21struct"

type gndTitleResult struct {
	Level   string   `json:"level,omitempty"`
	Type    []string `json:"entityType,omitempty"`
	UseFor  []string `json:"usefor,omitempty"`
	Variant []string `json:"variant,omitempty"`
	OtherId []string `json:"otherIdentifier,omitempty"`
}

func parseGNDTitle(rec *marc21struct.Record) *gndTitleResult {
	var result = &gndTitleResult{
		Level:   "",
		Type:    []string{},
		UseFor:  []string{},
		Variant: []string{},
		OtherId: []string{},
	}
	for _, df := range rec.Datafields {
		switch df.Tag {
		case "024":
			source := getSubfield(df.Subfields, "2")
			id := getSubfield(df.Subfields, "a")
			if len(source) == 1 && len(id) == 1 {
				for index, s := range source {
					if s != "gnd" {
						result.OtherId = append(result.OtherId, "("+s+")"+id[index])
					}
				}
			}
		case "042":
			levels := getSubfield(df.Subfields, "a")
			if len(levels) > 0 {
				result.Level = levels[0]
			}
		case "075":
			result.Type = append(result.Type, getSubfield(df.Subfields, "b")...)
		case "079":
			result.UseFor = append(result.UseFor, getSubfield(df.Subfields, "q")...)
		case "400", "410":
			result.Variant = append(result.Variant, getSubfield(df.Subfields, "t")...)
			result.Variant = append(result.Variant, getSubfield(df.Subfields, "g")...)
			result.Variant = append(result.Variant, getSubfield(df.Subfields, "m")...)
			result.Variant = append(result.Variant, getSubfield(df.Subfields, "n")...)
			result.Variant = append(result.Variant, getSubfield(df.Subfields, "r")...)
			result.Variant = append(result.Variant, getSubfield(df.Subfields, "f")...)
			result.Variant = append(result.Variant, getSubfield(df.Subfields, "s")...)
			result.Variant = append(result.Variant, getSubfield(df.Subfields, "p")...)
			result.Variant = append(result.Variant, getSubfield(df.Subfields, "k")...)
			result.Variant = append(result.Variant, getSubfield(df.Subfields, "o")...)
		case "430":
			result.Variant = append(result.Variant, getSubfield(df.Subfields, "a")...)
			result.Variant = append(result.Variant, getSubfield(df.Subfields, "g")...)
			result.Variant = append(result.Variant, getSubfield(df.Subfields, "m")...)
			result.Variant = append(result.Variant, getSubfield(df.Subfields, "n")...)
			result.Variant = append(result.Variant, getSubfield(df.Subfields, "r")...)
			result.Variant = append(result.Variant, getSubfield(df.Subfields, "f")...)
			result.Variant = append(result.Variant, getSubfield(df.Subfields, "s")...)
			result.Variant = append(result.Variant, getSubfield(df.Subfields, "p")...)
			result.Variant = append(result.Variant, getSubfield(df.Subfields, "k")...)
			result.Variant = append(result.Variant, getSubfield(df.Subfields, "o")...)
		}
	}
	if len(result.Variant) == 0 {
		result.Variant = nil
	}
	if len(result.UseFor) == 0 {
		result.UseFor = nil
	}
	if len(result.Type) == 0 {
		result.Type = nil
	}

	return result
}
