package gndstructservice

import "gitlab.switch.ch/ub-unibas/gndservice/v2/pkg/marc21struct"

type gndCorporateResult struct {
	Level   string   `json:"level,omitempty"`
	Type    []string `json:"entityType,omitempty"`
	UseFor  []string `json:"usefor,omitempty"`
	Variant []string `json:"variant,omitempty"`
	Related []string `json:"related,omitempty"`
	OtherId []string `json:"otherIdentifier,omitempty"`
	Place   []string `json:"placeOfBusiness,omitempty"`
}

func parseGNDCorporate(rec *marc21struct.Record) *gndCorporateResult {
	var result = &gndCorporateResult{
		Level:   "",
		Type:    []string{},
		UseFor:  []string{},
		Variant: []string{},
		Related: []string{},
		OtherId: []string{},
		Place:   []string{},
	}
	for _, df := range rec.Datafields {
		switch df.Tag {
		case "024":
			source := getSubfield(df.Subfields, "2")
			id := getSubfield(df.Subfields, "a")
			if len(source) == 1 && len(id) == 1 {
				for index, s := range source {
					if s != "gnd" {
						result.OtherId = append(result.OtherId, "("+s+")"+id[index])
					}
				}
			}
		case "042":
			levels := getSubfield(df.Subfields, "a")
			if len(levels) > 0 {
				result.Level = levels[0]
			}
		case "075":
			result.Type = append(result.Type, getSubfield(df.Subfields, "b")...)
		case "079":
			result.UseFor = append(result.UseFor, getSubfield(df.Subfields, "q")...)
		case "410":
			result.Variant = append(result.Variant, getSubfield(df.Subfields, "a")...)
			result.Variant = append(result.Variant, getSubfield(df.Subfields, "b")...)
		case "451":
			result.Variant = append(result.Variant, getSubfield(df.Subfields, "a")...)
		case "510":
			relations := getSubfield(df.Subfields, "4")
			for _, r := range relations {
				if r == "vorg" || r == "nach" || r == "nazw" {
					result.Related = append(result.Related, getSubfield(df.Subfields, "a")...)
					result.Related = append(result.Related, getSubfield(df.Subfields, "b")...)
				}
			}
		case "551":
			relations := getSubfield(df.Subfields, "4")
			for _, r := range relations {
				if r == "vorg" || r == "nach" || r == "nazw" {
					result.Related = append(result.Related, getSubfield(df.Subfields, "a")...)
				}
				if r == "orta" {
					result.Place = append(result.Place, getSubfield(df.Subfields, "a")...)
				}
			}
		}
	}
	if len(result.Variant) == 0 {
		result.Variant = nil
	}
	if len(result.UseFor) == 0 {
		result.UseFor = nil
	}
	if len(result.Type) == 0 {
		result.Type = nil
	}

	return result
}
