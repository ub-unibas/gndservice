definitions:
  gndstructservice.HTTPResultMessage:
    properties:
      code:
        example: 400
        type: integer
      message:
        example: status bad request
        type: string
    type: object
  marc21struct.Controlfield:
    properties:
      tag:
        type: string
      text:
        type: string
    type: object
  marc21struct.Datafield:
    properties:
      ind1:
        type: string
      ind2:
        type: string
      subfield:
        items:
          $ref: '#/definitions/marc21struct.Subfield'
        type: array
      tag:
        type: string
    type: object
  marc21struct.QueryStruct:
    properties:
      LDR:
        type: string
      controlfield:
        items:
          $ref: '#/definitions/marc21struct.Controlfield'
        type: array
      datafield:
        items:
          $ref: '#/definitions/marc21struct.Datafield'
        type: array
      field:
        $ref: '#/definitions/marc21struct.Datafield'
      name:
        type: string
    type: object
  marc21struct.Subfield:
    properties:
      code:
        type: string
      datafield:
        $ref: '#/definitions/marc21struct.Datafield'
      text:
        type: string
    type: object
info:
  contact:
    email: it-ub@unibas.ch
    name: University Library Basel, Informatik
    url: https://ub.unibas.ch
  description: This API is a wrapper around the GND Service that enriches MARC21 data
    with GND data. The MARC21 data has to be supplied in a specific structure for
    it to work with the relevant fields. For each GND linked entity one enriched entity
    is returned.
  license:
    name: Apache 2.0
    url: http://www.apache.org/licenses/LICENSE-2.0.html
  termsOfService: http://swagger.io/terms/
  title: GND Structure Enrichment API
  version: "1.0"
paths:
  /conference:
    post:
      description: Takes the relevant MARC data of an entry and returns an enriched
        conference structure
      parameters:
      - description: 'Get '
        in: body
        name: MarcQueryStruct
        required: true
        schema:
          $ref: '#/definitions/marc21struct.QueryStruct'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            items:
              type: object
            type: array
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
      security:
      - ApiKeyAuth: []
      summary: conference enrichment
      tags:
      - GND
  /corporate:
    post:
      description: Takes the relevant MARC data of an entry and returns an enriched
        corporate structure
      parameters:
      - description: 'Get '
        in: body
        name: MarcQueryStruct
        required: true
        schema:
          $ref: '#/definitions/marc21struct.QueryStruct'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            items:
              type: object
            type: array
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
      security:
      - ApiKeyAuth: []
      summary: corporate enrichment
      tags:
      - GND
  /family:
    post:
      description: Takes the relevant MARC data of an entry and returns an enriched
        family structure
      parameters:
      - description: 'Get '
        in: body
        name: MarcQueryStruct
        required: true
        schema:
          $ref: '#/definitions/marc21struct.QueryStruct'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            items:
              type: object
            type: array
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
      security:
      - ApiKeyAuth: []
      summary: family enrichment
      tags:
      - GND
  /geo:
    post:
      description: Takes the relevant MARC data of an entry and returns an enriched
        geo structure
      parameters:
      - description: 'Get '
        in: body
        name: MarcQueryStruct
        required: true
        schema:
          $ref: '#/definitions/marc21struct.QueryStruct'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            items:
              type: object
            type: array
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
      security:
      - ApiKeyAuth: []
      summary: geo enrichment
      tags:
      - GND
  /person:
    post:
      description: Takes the relevant MARC data of an entry and returns an enriched
        person structure
      parameters:
      - description: 'Get '
        in: body
        name: MarcQueryStruct
        required: true
        schema:
          $ref: '#/definitions/marc21struct.QueryStruct'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            items:
              type: object
            type: array
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
      security:
      - ApiKeyAuth: []
      summary: person enrichment
      tags:
      - GND
  /subject.conference:
    post:
      description: Takes the relevant MARC data of an entry and returns an enriched
        conference structure
      parameters:
      - description: 'Get '
        in: body
        name: MarcQueryStruct
        required: true
        schema:
          $ref: '#/definitions/marc21struct.QueryStruct'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            items:
              type: object
            type: array
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
      security:
      - ApiKeyAuth: []
      summary: conference from subject field enrichment
      tags:
      - GND
  /subject.corporate:
    post:
      description: Takes the relevant MARC data of an entry and returns an enriched
        corporate structure
      parameters:
      - description: 'Get '
        in: body
        name: MarcQueryStruct
        required: true
        schema:
          $ref: '#/definitions/marc21struct.QueryStruct'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            items:
              type: object
            type: array
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
      security:
      - ApiKeyAuth: []
      summary: corporate from subject enrichment
      tags:
      - GND
  /subject.family:
    post:
      description: Takes the relevant MARC data of an entry and returns an enriched
        family structure
      parameters:
      - description: 'Get '
        in: body
        name: MarcQueryStruct
        required: true
        schema:
          $ref: '#/definitions/marc21struct.QueryStruct'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            items:
              type: object
            type: array
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
      security:
      - ApiKeyAuth: []
      summary: family subject enrichment
      tags:
      - GND
  /subject.geo:
    post:
      description: Takes the relevant MARC data of an entry and returns an enriched
        geo structure
      parameters:
      - description: 'Get '
        in: body
        name: MarcQueryStruct
        required: true
        schema:
          $ref: '#/definitions/marc21struct.QueryStruct'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            items:
              type: object
            type: array
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
      security:
      - ApiKeyAuth: []
      summary: geo from subject enrichment
      tags:
      - GND
  /subject.person:
    post:
      description: Takes the relevant MARC data of an entry and returns an enriched
        person structure
      parameters:
      - description: 'Get '
        in: body
        name: MarcQueryStruct
        required: true
        schema:
          $ref: '#/definitions/marc21struct.QueryStruct'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            items:
              type: object
            type: array
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
      security:
      - ApiKeyAuth: []
      summary: get personSubject
      tags:
      - GND
  /subject.title:
    post:
      description: Takes the relevant MARC data of an entry and returns an enriched
        title structure
      parameters:
      - description: 'Get '
        in: body
        name: MarcQueryStruct
        required: true
        schema:
          $ref: '#/definitions/marc21struct.QueryStruct'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            items:
              type: object
            type: array
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
      security:
      - ApiKeyAuth: []
      summary: title from subject enrichment
      tags:
      - GND
  /title:
    post:
      description: Takes the relevant MARC data of an entry and returns an enriched
        title structure
      parameters:
      - description: 'Get '
        in: body
        name: MarcQueryStruct
        required: true
        schema:
          $ref: '#/definitions/marc21struct.QueryStruct'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            items:
              type: object
            type: array
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
      security:
      - ApiKeyAuth: []
      summary: title enrichment
      tags:
      - GND
  /topic:
    post:
      description: Takes the relevant MARC data of an entry and returns an enriched
        topic structure
      parameters:
      - description: 'Get '
        in: body
        name: MarcQueryStruct
        required: true
        schema:
          $ref: '#/definitions/marc21struct.QueryStruct'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            items:
              type: object
            type: array
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/gndstructservice.HTTPResultMessage'
      security:
      - ApiKeyAuth: []
      summary: topic enrichment
      tags:
      - GND
securityDefinitions:
  ApiKeyAuth:
    description: Bearer Authentication with JWT
    in: header
    name: Authorization
    type: apiKey
swagger: "2.0"
