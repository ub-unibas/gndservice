package gndstructservice

import (
	"gitlab.switch.ch/ub-unibas/gndservice/v2/pkg/marc21struct"
	"strings"
)

type gndGeoCoordinates struct {
	Lat string `json:"lat,omitempty"`
	Lon string `json:"lon,omitempty"`
}

type gndGeoResult struct {
	Level       string              `json:"level,omitempty"`
	Type        []string            `json:"entityType,omitempty"`
	UseFor      []string            `json:"usefor,omitempty"`
	Variant     []string            `json:"variant,omitempty"`
	Related     []string            `json:"related,omitempty"`
	Coordinates []gndGeoCoordinates `json:"coordinates,omitempty"`
	GeoNamesId  []string            `json:"geoNamesId,omitempty"`
	OtherId     []string            `json:"otherIdentifier,omitempty"`
}

func parseGNDGeo(rec *marc21struct.Record) *gndGeoResult {
	var result = &gndGeoResult{
		Level:       "",
		Type:        []string{},
		UseFor:      []string{},
		Variant:     []string{},
		Related:     []string{},
		Coordinates: []gndGeoCoordinates{},
		GeoNamesId:  []string{},
		OtherId:     []string{},
	}
	for _, df := range rec.Datafields {
		switch df.Tag {
		case "024":
			source := getSubfield(df.Subfields, "2")
			id := getSubfield(df.Subfields, "a")
			if len(source) == 1 && len(id) == 1 {
				for index, s := range source {
					if s != "gnd" {
						result.OtherId = append(result.OtherId, "("+s+")"+id[index])
					}
				}
			}
		case "042":
			levels := getSubfield(df.Subfields, "a")
			if len(levels) > 0 {
				result.Level = levels[0]
			}
		case "075":
			result.Type = append(result.Type, getSubfield(df.Subfields, "b")...)
		case "079":
			result.UseFor = append(result.UseFor, getSubfield(df.Subfields, "q")...)
		case "451":
			result.Variant = append(result.Variant, getSubfield(df.Subfields, "a")...)
		case "551":
			relations := getSubfield(df.Subfields, "4")
			for _, r := range relations {
				if r == "vorg" || r == "nach" || r == "nazw" {
					result.Related = append(result.Related, getSubfield(df.Subfields, "a")...)
				}
			}
		case "034":
			types := getSubfield(df.Subfields, "9")
			for _, t := range types {
				if strings.HasPrefix(t, "A:d") {
					sources := getSubfield(df.Subfields, "2")
					for _, s := range sources {
						if s == "geonames" {
							coords := gndGeoCoordinates{
								Lat: strings.Replace(strings.TrimLeft(getSubfield(df.Subfields, "f")[0], "N"), "S", "-", 1),
								Lon: strings.Replace(strings.TrimLeft(getSubfield(df.Subfields, "d")[0], "E"), "W", "-", 1),
							}
							result.Coordinates = append(result.Coordinates, coords)
							result.GeoNamesId = append(result.GeoNamesId, getSubfield(df.Subfields, "0")...)
						}
					}
				}
			}
		}
	}
	if len(result.Variant) == 0 {
		result.Variant = nil
	}
	if len(result.UseFor) == 0 {
		result.UseFor = nil
	}
	if len(result.Type) == 0 {
		result.Type = nil
	}
	if len(result.Related) == 0 {
		result.Related = nil
	}
	if len(result.Coordinates) == 0 {
		result.Coordinates = nil
	}
	if len(result.GeoNamesId) == 0 {
		result.GeoNamesId = nil
	}

	return result
}
