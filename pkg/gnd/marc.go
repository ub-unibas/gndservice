package gnd

import "gitlab.switch.ch/ub-unibas/gndservice/v2/pkg/marc21struct"

// Leader is for containing the text string of the MARC record Leader
type Leader struct {
	Text string `xml:",chardata" json:"text" example:"00000nz  a2200000oc 4500"`
}

// Controlfield contains a controlfield entry
type Controlfield struct {
	Tag  string `xml:"tag,attr" json:"tag" example:"001"`
	Text string `xml:",chardata" json:"text" example:"1298776236"`
	Type string `xml:"-" json:"-"`
}

type Controlfields []*Controlfield

// Subfield contains a subfield entry
type Subfield struct {
	Code string `xml:"code,attr" json:"code" example:"2"`
	Text string `xml:",chardata" json:"text" example:"gndgen"`
	//	Datafield *Datafield
}

// Datafield contains a datafield entry
type Datafield struct {
	Tag       string      `xml:"tag,attr" json:"tag" example:"075"`
	Ind1      string      `xml:"ind1,attr" json:"ind1,omitempty" example:" "`
	Ind2      string      `xml:"ind2,attr" json:"ind2,omitempty" example:" "`
	Subfields []*Subfield `xml:"subfield" json:"subfield,omitempty"`
}

// Record is for containing a MARC record
type _Record struct {
	Leader        Leader        `xml:"leader" json:"LDR"`
	Controlfields Controlfields `xml:"controlfield" json:"controlfield"`
	Datafields    []*Datafield  `xml:"datafield" json:"datafield"`
}

func GetControlfield(doc *marc21struct.Record, tag string) []string {
	if doc.Controlfields == nil {
		return nil
	}
	var result = []string{}
	for _, cf := range doc.Controlfields {
		if cf.Tag == tag || tag == "" {
			result = append(result, cf.Text)
		}
	}
	return result
}

func GetSubfield(doc *marc21struct.Record, tag, ind1, ind2, code string) []string {
	var result = []string{}
	if doc.Datafields == nil {
		return nil
	}
	for _, datafield := range doc.Datafields {
		if datafield.Tag != tag && tag != "" {
			continue
		}
		if datafield.Ind1 != ind1 && ind1 != "" {
			continue
		}
		if datafield.Ind2 != ind2 && ind2 != "" {
			continue
		}
		if datafield.Subfields == nil {
			continue
		}
		for _, subfield := range datafield.Subfields {
			if subfield.Code != code && code != "" {
				continue
			}
			result = append(result, subfield.Text)
		}
	}
	return result
}
