package gnd

import (
	"bytes"
	"emperror.dev/errors"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"github.com/andybalholm/brotli"
	"github.com/dgraph-io/badger/v4"
	"github.com/miku/metha"
	"gitlab.switch.ch/ub-unibas/alma2elastic/v2/pkg/oaipmh"
	"gitlab.switch.ch/ub-unibas/gndservice/v2/pkg/marc21struct"
	"os"
	"path/filepath"
	"strings"
	"time"
)

const LastSyncDataFileName = "lastsync.dat"

func (gnd *Database) Iterate() error {
	statusFilename := filepath.Join(gnd.databaseDirectory, LastSyncDataFileName)
	var from time.Time
	fromData, err := os.ReadFile(statusFilename)
	if err != nil {
		return errors.Wrapf(err, "cannot read file last sync status file '%s'", statusFilename)
	}
	from, err = time.Parse(SyncTimeLayout, string(fromData))
	if err != nil {
		return errors.Wrapf(err, "cannot parse date '%s'", fromData)
	}
	now := time.Now()
	if err := gnd.iterateOAI(from); err != nil {
		return errors.Wrapf(err, "cannot harvest authority '%s'", gnd.authority)
	}
	if err := os.WriteFile(statusFilename, []byte(now.Format(SyncTimeLayout)), 0666); err != nil {
		return errors.Wrapf(err, "cannot write status file '%s'", statusFilename)
	}
	return nil
}

func (gnd *Database) iterateOAI(from time.Time) error {
	type kv struct {
		key, value []byte
	}
	var objectCounter int64
	var keyCounter int64
	const maxFields int64 = 1000
	var kvList = make([]*kv, 0, maxFields)
	var start = time.Now()
	/*
		bb, err := badgerBuffer.NewBadgerBuffer(1000, gnd.db, gnd.syncCond, gnd.inSync)
		if err != nil {
			return errors.Wrap(err, "cannot create badger buffer")
		}
		defer func() { bb.Flush() }()
	*/

	doUpdate := func() error {
		gnd.mutex.Lock()
		defer gnd.mutex.Unlock()
		gnd.readTransaction.Discard()
		defer func() {
			gnd.readTransaction = gnd.db.NewTransaction(false)
		}()
		err := gnd.db.Update(func(txn *badger.Txn) error {
			for _, v := range kvList {
				if err := txn.Set(v.key, v.value); err != nil {
					return errors.Wrapf(err, "cannot store key '%s'", v.key)
				}
			}
			clear(kvList)
			kvList = make([]*kv, 0, maxFields)
			return nil
		})
		return errors.WithStack(err)
	}
	defer doUpdate()
	//todo: sync in parts
	harvest, err := oaipmh.NewHarvester(
		gnd.oaiURL,
		"MARC21-xml",
		gnd.oaiSet,
		200000,
		1,
		time.Second,
		gnd.logger)
	if err != nil {
		return errors.Wrapf(err, "cannot harvest '%s'", gnd.dumpBaseUrl)
	}

	for now := time.Now(); from.Before(now); from = from.Add(gnd.interval) {
		gnd.logger.Debug().Msgf("Harvesting from '%s' to '%s'", from.UTC().String(), from.Add(gnd.interval).UTC().String())
		if err := harvest.Harvest(from.Add(-time.Hour).UTC(), from.Add(gnd.interval).UTC(), func(recs []metha.Record) error {
			for _, rec := range recs {
				var doc = &marc21struct.Record{}

				//var docID = rec.Header.Identifier

				if rec.Header.Status == "deleted" {
					// todo: delete something
					continue
				}

				buf := bytes.NewBuffer(rec.Metadata.Body)
				dec := xml.NewDecoder(buf)
				if err = dec.Decode(doc); err != nil {
					return errors.Wrapf(err, "cannot parse metadata: %s", rec.Metadata.GoString())
				}

				keys := GetControlfield(doc, "001")
				if len(keys) == 0 {
					gnd.logger.Warn().Msgf("%s has no controlfield 001", rec.Header.Identifier)
					continue
				}
				key := keys[0]
				jsondata, err := json.Marshal(doc)
				if err != nil {
					return errors.Wrapf(err, "cannot marshal json for identifier '%s'", rec.Header.Identifier)
				}

				var dataBuf = &bytes.Buffer{}
				wr := brotli.NewWriterLevel(dataBuf, brotli.BestSpeed)
				if _, err := wr.Write(jsondata); err != nil {
					return errors.Wrap(err, "cannot write xml to brotli")
				}
				wr.Close()
				//				gnd.logger.Debugf("key: %s", key)
				/*
					if err := bb.Add([]byte(strings.ToUpper(key)), dataBuf.Bytes()); err != nil {
						return errors.Wrap(err, "cannot add value")
					}
				*/
				kvList = append(kvList, &kv{[]byte(strings.ToUpper(key)), dataBuf.Bytes()})
				if len(kvList) >= int(maxFields) {
					if err := doUpdate(); err != nil {
						return errors.Wrap(err, "cannot update badger")
					}
				}
				objectCounter++

				gnds := GetSubfield(doc, "035", " ", " ", "a")
				if gnds == nil {
					continue
				}
				for _, g := range gnds {
					var gKey = fmt.Sprintf("%s.%s", g, key)
					//					gnd.logger.Debugf("key: %s", gKey)
					kvList = append(kvList, &kv{[]byte(strings.ToUpper(gKey)), nil})
					if len(kvList) >= int(maxFields) {
						if err := doUpdate(); err != nil {
							return errors.Wrap(err, "cannot update badger")
						}
					}
					keyCounter++
				}
				if objectCounter%1000 == 0 {
					since := time.Since(start)
					since /= time.Second
					num_sec := objectCounter / int64(since)
					gnd.logger.Info().Msgf("%7d // %7d - %d/sec - %s", objectCounter, keyCounter, num_sec, gnd.oaiSet)
					//				start = time.Now()
				}
			}
			return nil
		}); err != nil {
			return errors.Wrapf(err, "cannot harvest '%s'", gnd.oaiURL)
		}
	}
	return nil
}
