package gnd

import (
	"bytes"
	"context"
	"emperror.dev/errors"
	"encoding/json"
	"github.com/andybalholm/brotli"
	"github.com/dgraph-io/badger/v4"
	"gitlab.switch.ch/ub-unibas/gndservice/v2/pkg/marc21struct"
	ublogger "gitlab.switch.ch/ub-unibas/go-ublogger/v2"
	"io"
	"strings"
	"sync"
	"sync/atomic"
	"time"
)

func NewDatabase(databaseDirectory, dumpTargetDirectory, dumpBaseUrl, oaiBaseUrl, authority string, interval time.Duration, logger *ublogger.Logger) (*Database, error) {
	var err error
	var oaiSet string
	if authority != "" {
		oaiSet = "authorities:" + authority
	} else {
		oaiSet = "authorities"
	}
	var gnd = &Database{
		dumpBaseUrl:         dumpBaseUrl,
		dumpTargetDirectory: dumpTargetDirectory,
		databaseDirectory:   databaseDirectory,
		oaiURL:              oaiBaseUrl,
		oaiSet:              oaiSet,
		authority:           authority,
		interval:            interval,
		logger:              logger,
		syncCond:            sync.NewCond(&sync.Mutex{}),
		inSync:              &atomic.Bool{},
		mutex:               sync.RWMutex{},
	}
	gnd.db, err = badger.Open(badger.DefaultOptions(databaseDirectory))
	if err != nil {
		return nil, errors.Wrapf(err, "cannot open badger in '%s'", databaseDirectory)
	}
	gnd.readTransaction = gnd.db.NewTransaction(false)
	return gnd, nil
}

type Database struct {
	db                  *badger.DB
	dumpBaseUrl         string
	dumpTargetDirectory string
	oaiURL              string
	oaiSet              string
	authority           string
	databaseDirectory   string
	logger              *ublogger.Logger
	interval            time.Duration
	syncCond            *sync.Cond
	inSync              *atomic.Bool
	readTransaction     *badger.Txn
	mutex               sync.RWMutex
}

func (gnd *Database) Close() error {
	return errors.WithStack(gnd.db.Close())
}

func IsNotFound(err error) bool {
	return errors.Is(err, badger.ErrKeyNotFound)
}

func (gnd *Database) Sync(wait time.Duration, wg *sync.WaitGroup) context.CancelFunc {
	wg.Add(1)
	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		defer wg.Done()
		for {
			if err := gnd.Iterate(); err != nil {
				gnd.logger.Error().Msgf("cannot sync: %v", err)
			}
			gnd.logger.Info().Msgf("sleeping %s", wait.String())
			select {
			case <-time.After(wait):

			case <-ctx.Done():
				gnd.logger.Info().Msgf("sync loop cancelled")
				return
			}
		}
	}()
	return cancel
}

func (gnd *Database) Get(identifier string) (*marc21struct.Record, error) {
	gnd.mutex.RLock()
	defer gnd.mutex.RUnlock()
	var value = &marc21struct.Record{}
	var valKey string
	if strings.HasPrefix(identifier, "(") {
		opts := badger.DefaultIteratorOptions
		opts.PrefetchValues = false
		opts.Reverse = false
		it := gnd.readTransaction.NewIterator(opts)

		prefix := []byte(identifier + ".")
		for it.Seek(prefix); it.ValidForPrefix(prefix); it.Next() {
			item := it.Item()
			k := item.Key()
			parts := strings.Split(string(k), ".")
			if len(parts) != 2 {
				it.Close()
				return nil, errors.Errorf("invalid key '%s' for prefix search '%s'", string(k), prefix)
			}
			valKey = parts[1]
		}
		it.Close()
		if valKey == "" {
			return nil, errors.Wrapf(badger.ErrKeyNotFound, "could not find key prefix '%s'", prefix)
		}
	} else {
		valKey = identifier
	}
	it, err := gnd.readTransaction.Get([]byte(valKey))
	if err != nil { // Key not found
		return nil, errors.Wrapf(err, "cannot load value for key '%s' (%s)", valKey, identifier)
	}

	if err := it.Value(func(val []byte) error {
		var buf = bytes.NewBuffer(val)
		br := brotli.NewReader(buf)
		var data = bytes.Buffer{}
		if _, err := io.Copy(&data, br); err != nil {
			return errors.Wrapf(err, "cannot read decompressed value for key '%s' (%s)", valKey, identifier)
		}
		if err := json.Unmarshal(data.Bytes(), value); err != nil {
			return errors.Wrapf(err, "cannot unmarshal json for key '%s' (%s)", valKey, identifier)
		}
		return nil
	}); err != nil {
		return nil, errors.Wrapf(err, "cannot get value for key '%s' (%s)", valKey, identifier)
	}
	return value, nil
}
