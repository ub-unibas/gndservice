package gnd

import (
	"bytes"
	"emperror.dev/errors"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"github.com/andybalholm/brotli"
	"github.com/je4/utils/v2/pkg/badgerBuffer"
	"gitlab.switch.ch/ub-unibas/gndservice/v2/pkg/marc21struct"
	"io"
	"strings"
	"time"
)

const SyncTimeLayout = time.RFC3339

func (gnd *Database) parse(reader io.Reader, filename string) error {
	xmlDecoder := xml.NewDecoder(reader)
	badgerBugger, err := badgerBuffer.NewBadgerBuffer(5000, gnd.db, gnd.syncCond, gnd.inSync)
	if err != nil {
		return errors.Wrap(err, "cannot create new badger buffer")
	}
	defer func() {
		flushError := badgerBugger.Flush()
		if flushError != nil {
			gnd.logger.Error().Msgf("cannot flush badger buffer: %v", flushError)
			return
		}
	}()
	var objectCounter int64
	var keyCounter int64
	var start = time.Now()

	for token, _ := xmlDecoder.Token(); token != nil; token, _ = xmlDecoder.Token() {
		switch startElement := token.(type) {
		case xml.StartElement:
			if startElement.Name.Local == "record" {
				var doc = &marc21struct.Record{}
				if err := xmlDecoder.DecodeElement(doc, &startElement); err != nil {
					continue
				}

				keys := GetControlfield(doc, "001")
				if len(keys) == 0 {
					gnd.logger.Warn().Msgf("%s has no controlfield 001", doc.Leader.Text)
					continue
				}
				key := keys[0]

				jsondata, err := json.Marshal(doc)
				if err != nil {
					return errors.Wrapf(err, "cannot marshal json for identifier '%s'", doc.Leader.Text)
				}

				var dataBuf = &bytes.Buffer{}
				wr := brotli.NewWriterLevel(dataBuf, brotli.BestSpeed)
				if _, err := wr.Write(jsondata); err != nil {
					return errors.Wrap(err, "cannot write xml to brotli")
				}
				_ = wr.Close()
				if err := badgerBugger.Add([]byte(strings.ToUpper(key)), dataBuf.Bytes()); err != nil {
					return errors.Wrapf(err, "cannot store identifier '%s'", doc.Leader.Text)
				}
				objectCounter++

				gnds := GetSubfield(doc, "035", " ", " ", "a")
				if gnds == nil {
					continue
				}
				for _, g := range gnds {
					var gKey = fmt.Sprintf("%s.%s", g, key)
					if err := badgerBugger.Add([]byte(strings.ToUpper(gKey)), nil); err != nil {
						return errors.Wrapf(err, "cannot store reference to identifier '%s'", doc.Leader.Text)
					}
					keyCounter++
				}
				if objectCounter%1000 == 0 {
					since := time.Since(start)
					since /= time.Second
					if int64(since) > 0 {
						numSec := objectCounter / int64(since)
						gnd.logger.Info().Msgf("%7d // %7d - %d/sec - %s", objectCounter, keyCounter, numSec, filename)
					}
					//start = time.Now()
				}
			}
		}
	}
	return nil
}
