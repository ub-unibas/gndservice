package gnd

import (
	"bufio"
	"compress/gzip"
	"emperror.dev/errors"
	"github.com/je4/utils/v2/pkg/checksum"
	"io"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"time"
)

const ChecksumFileName = "001_Pruefsumme_Checksum.txt"

var dateFromFilenameRegex = regexp.MustCompile(`_([0-9]{8})\.mrc\.xml\.gz$`)

func (gnd *Database) ImportData(drop bool) error {
	if drop {
		if err := gnd.db.DropAll(); err != nil {
			return errors.WithStack(err)
		}
	}
	_, err := os.Stat(gnd.dumpTargetDirectory)
	if err != nil {
		if os.IsNotExist(err) {
			if err := os.MkdirAll(gnd.dumpTargetDirectory, 0777); err != nil {
				return errors.Wrapf(err, "cannot create dump directory '%s'", gnd.dumpTargetDirectory)
			}
		} else {
			return errors.Wrapf(err, "cannot stat '%s'", gnd.dumpTargetDirectory)
		}
	} else {
		if drop {
			err := os.RemoveAll(gnd.dumpTargetDirectory)
			if err != nil {
				return err
			}
		} else {
			gnd.logger.Info().Msgf("dump already imported as directory %s already exists.", gnd.dumpTargetDirectory)
			return nil
		}
	}

	initialFiles, from, err := gnd.getFileNames()
	if err != nil {
		return errors.Wrap(err, "cannot get initial files")
	}

	for _, fileName := range initialFiles {
		gnd.logger.Info().Msgf("downloading '%s'", fileName)
		if err := downloadFile(gnd.dumpBaseUrl, fileName, gnd.dumpTargetDirectory); err != nil {
			return errors.Wrapf(err, "cannot download '%s'", fileName)
		}
		if err := gnd.parseFile(gnd.dumpTargetDirectory, fileName); err != nil {
			return errors.Wrapf(err, "cannot parse '%s'", fileName)
		}
	}
	statusFilename := filepath.Join(gnd.databaseDirectory, LastSyncDataFileName)
	if writeFileError := os.WriteFile(statusFilename, []byte(from.Format(SyncTimeLayout)), 0666); writeFileError != nil {
		gnd.logger.Error().Msgf("cannot write status file '%s': %v", statusFilename, writeFileError)
	}
	return nil
}

func (gnd *Database) getFileNames() (map[string]string, time.Time, error) {
	checksumFileUrl, err := url.JoinPath(gnd.dumpBaseUrl, ChecksumFileName)
	if err != nil {
		return nil, time.Time{}, errors.Wrapf(err, "cannot join url '%s' with '%s'", gnd.dumpBaseUrl, ChecksumFileName)
	}
	resp, err := http.Get(checksumFileUrl)
	if err != nil {
		return nil, time.Time{}, errors.Wrapf(err, "cannot get '%s'", checksumFileUrl)
	}
	defer resp.Body.Close()

	fileScanner := bufio.NewScanner(resp.Body)
	fileScanner.Split(bufio.ScanLines)
	var result = map[string]string{}
	var oldest time.Time
	for fileScanner.Scan() {
		line := strings.Fields(fileScanner.Text())
		if len(line) < 2 {
			continue
		}
		file := strings.TrimSpace(line[0])
		found := dateFromFilenameRegex.FindStringSubmatch(file)
		if found == nil {
			// ignore files that do not match the expected pattern
			continue
		}
		checksumDigits := strings.TrimSpace(line[1])
		result[checksumDigits] = file
		parsedTime, timeParseError := time.Parse("20060102", found[1])
		if timeParseError != nil {
			return nil, time.Time{}, errors.Wrapf(timeParseError, "cannot parse time of filename '%s'", file)
		}
		if oldest.IsZero() || oldest.After(parsedTime) {
			oldest = parsedTime
		}
	}
	return result, oldest, nil
}

func downloadFile(dumpBaseUrl, fileName, targetDir string) error {
	targetFilename := filepath.Join(targetDir, fileName)
	_, err := os.Stat(targetFilename)
	if os.IsNotExist(err) {
		fileUrl, joinPathError := url.JoinPath(dumpBaseUrl, fileName)
		if joinPathError != nil {
			return errors.Wrapf(joinPathError, "cannot join url '%s' with '%s'", dumpBaseUrl, fileName)
		}
		response, httpError := http.Get(fileUrl)
		if httpError != nil {
			return errors.Wrapf(httpError, "cannot get '%s'", fileUrl)
		}
		fileObject, fileCreationError := os.Create(targetFilename)
		if fileCreationError != nil {
			return errors.Wrapf(fileCreationError, "cannot create '%s'", targetFilename)
		}
		checksumWriter, _ := checksum.NewChecksumWriter(
			[]checksum.DigestAlgorithm{checksum.DigestSHA256},
			fileObject,
		)
		if _, checksumError := io.Copy(checksumWriter, response.Body); checksumError != nil {
			_ = response.Body.Close()
			_ = fileObject.Close()
			_ = checksumWriter.Close()
			return errors.Wrapf(checksumError, "cannot write '%s'", targetFilename)
		}
		_ = response.Body.Close()
		_ = fileObject.Close()
		_ = checksumWriter.Close()

	} else {
		if err != nil {
			return errors.Wrapf(err, "cannot stat '%s'", targetFilename)
		} else {

			return nil
		}
	}
	return nil

}

func (gnd *Database) parseFile(fileDirectory, filename string) error {
	targetFilename := filepath.Join(fileDirectory, filename)
	fp, err := os.Open(targetFilename)
	if err != nil {
		_ = os.Remove(targetFilename)
		return errors.Wrapf(err, "cannot open '%s'", targetFilename)
	}
	defer func() {
		_ = fp.Close()
		_ = os.Remove(targetFilename)
	}()
	reader, err := gzip.NewReader(fp)
	if err != nil {
		_ = os.Remove(targetFilename)
		return errors.Wrapf(err, "cannot create gzip reader for '%s'", targetFilename)
	}
	br := bufio.NewReaderSize(reader, 65536)
	return errors.WithStack(gnd.parse(br, filename))
}
