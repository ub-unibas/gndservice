package gndservice

import (
	"crypto/tls"
	"gitlab.switch.ch/ub-unibas/gndservice/v2/pkg/gnd"
	ublogger "gitlab.switch.ch/ub-unibas/go-ublogger/v2"
	"sync"
)

type ShutdownService interface {
	Stop()
	GracefulStop()
}

func Startup(
	domain string,
	httpPort int,
	proxyUri string,
	tlsConfig *tls.Config,
	gnd *gnd.Database,
	wg *sync.WaitGroup,
	logger *ublogger.Logger) (ShutdownService, error) {
	ctrl, err := NewController(domain, httpPort, proxyUri, tlsConfig, gnd)
	if err != nil {
		logger.Panicf("Could not start controller: %v", err)
	}
	ctrl.Start(wg, tlsConfig != nil)
	logger.Info().Msgf("server (%s) started: %s/swagger/index.html", ctrl.server.Addr, proxyUri)
	return ctrl, nil
}
