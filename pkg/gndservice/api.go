//go:generate swag init --parseDependency  --parseInternal -g api.go

package gndservice

import (
	"context"
	"crypto/tls"
	"emperror.dev/errors"
	"fmt"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"gitlab.switch.ch/ub-unibas/gndservice/v2/pkg/gnd"
	"gitlab.switch.ch/ub-unibas/gndservice/v2/pkg/gndservice/docs"
	"golang.org/x/net/http2"
	"log"
	"net/http"
	"net/url"
	"strings"
	"sync"
)

const ApiBasePath = "/api/v1"

//	@title			GND API
//	@version		1.0
//	@description	API for GND data. The API retrieves GND data from a local database that is updated once every hour.
//	@termsOfService	http://swagger.io/terms/

//	@contact.name	University Library Basel, Informatik
//	@contact.url	https://ub.unibas.ch
//	@contact.email	it-ub@unibas.ch

//	@license.name	Apache 2.0
//	@license.url	http://www.apache.org/licenses/LICENSE-2.0.html

// @securityDefinitions.apikey	ApiKeyAuth
// @in							header
// @name						Authorization
// @description					Bearer Authentication with JWT

func NewController(domain string, httpPort int, proxyUri string, tlsConfig *tls.Config, gnd *gnd.Database) (*Controller, error) {
	c := &Controller{
		server:   nil,
		proxyUri: proxyUri,
		gnd:      gnd,
	}
	docs.SwaggerInfo.BasePath = "/" + strings.Trim(ApiBasePath, "/")

	proxyUrl, err := url.Parse(proxyUri)
	if err != nil {
		return nil, errors.Wrapf(err, "invalid proxy uri '%s'", proxyUri)
	}
	if proxyUrl.Port() == "" {
		docs.SwaggerInfo.Host = proxyUrl.Hostname()
	} else {
		docs.SwaggerInfo.Host = fmt.Sprintf("%s:%s", proxyUrl.Hostname(), proxyUrl.Port())
	}
	docs.SwaggerInfo.Schemes = []string{proxyUrl.Scheme}
	if errHttpServer := c.initHttpServer(domain, httpPort, tlsConfig); errHttpServer != nil {
		return nil, errors.Wrap(errHttpServer, "could not initialize http server")
	}

	return c, nil
}

type Controller struct {
	server   *http.Server
	proxyUri string
	gnd      *gnd.Database
}

func (ctrl *Controller) initHttpServer(domain string, port int, tlsConfig *tls.Config) error {
	router := gin.Default()
	v1 := router.Group(ApiBasePath)
	v1.GET("/:identifier", ctrl.getGND)
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	router.NoRoute(func(c *gin.Context) {
		c.JSON(http.StatusNotFound, gin.H{
			"message": "Not found",
		})
	})
	ctrl.server = &http.Server{
		Addr:      fmt.Sprintf("%s:%d", domain, port),
		Handler:   router,
		TLSConfig: tlsConfig,
	}
	if err := http2.ConfigureServer(ctrl.server, nil); err != nil {
		return errors.WithStack(err)
	}
	return nil
}

func (ctrl *Controller) Start(wg *sync.WaitGroup, tlsEnabled bool) {
	go func() {
		wg.Add(1)
		defer wg.Done()
		if tlsEnabled {
			if err := ctrl.server.ListenAndServeTLS("", ""); err != nil {
				log.Fatalf("https server (%s) could not be started: %v", ctrl.server.Addr, err)
			}
		} else {
			if err := ctrl.server.ListenAndServe(); err != nil {
				log.Fatalf("http server (%s) could not be started: %v", ctrl.server.Addr, err)
			}
		}
	}()
}

func (ctrl *Controller) Stop() {
	err := ctrl.server.Shutdown(context.Background())
	if err != nil {
		return
	}
}

func (ctrl *Controller) GracefulStop() {
	err := ctrl.server.Shutdown(context.Background())
	if err != nil {
		return
	}
}

// newMediaItem godoc
// @Summary      get GND data
// @Description  Get a number of GND fields in JSON format from the local database for the given identifier
// @Security 	 ApiKeyAuth
// @Tags         GND
// @Produce      json
// @Param		 identifier path string true "GND identifier (with or without prefix)"
// @Success      200  {object}  marc21struct.Record
// @Failure      400  {object}  gndservice.HTTPResultMessage
// @Failure      404  {object}  gndservice.HTTPResultMessage
// @Failure      500  {object}  gndservice.HTTPResultMessage
// @Router       /{identifier} [get]
func (ctrl *Controller) getGND(c *gin.Context) {
	identifier := strings.ToUpper(c.Param("identifier"))
	item, err := ctrl.gnd.Get(identifier)
	if err != nil {
		if gnd.IsNotFound(err) {
			NewResultMessage(c, http.StatusNotFound, errors.Errorf("cannot find '%s' ", identifier))
		} else {
			NewResultMessage(c, http.StatusInternalServerError, errors.Errorf("error loading '%s' ", identifier))
		}
		return
	}
	if c.Query("format") == "xml" {
		c.XML(http.StatusOK, item)
		return
	}
	c.JSON(http.StatusOK, item)
}
