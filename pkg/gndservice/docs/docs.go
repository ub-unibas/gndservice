// Package docs Code generated by swaggo/swag. DO NOT EDIT
package docs

import "github.com/swaggo/swag"

const docTemplate = `{
    "schemes": {{ marshal .Schemes }},
    "swagger": "2.0",
    "info": {
        "description": "{{escape .Description}}",
        "title": "{{.Title}}",
        "termsOfService": "http://swagger.io/terms/",
        "contact": {
            "name": "University Library Basel, Informatik",
            "url": "https://ub.unibas.ch",
            "email": "it-ub@unibas.ch"
        },
        "license": {
            "name": "Apache 2.0",
            "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
        },
        "version": "{{.Version}}"
    },
    "host": "{{.Host}}",
    "basePath": "{{.BasePath}}",
    "paths": {
        "/{identifier}": {
            "get": {
                "security": [
                    {
                        "ApiKeyAuth": []
                    }
                ],
                "description": "Get a number of GND fields in JSON format from the local database for the given identifier",
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "GND"
                ],
                "summary": "get GND data",
                "parameters": [
                    {
                        "type": "string",
                        "description": "GND identifier (with or without prefix)",
                        "name": "identifier",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/marc21struct.Record"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/gndservice.HTTPResultMessage"
                        }
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "$ref": "#/definitions/gndservice.HTTPResultMessage"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/gndservice.HTTPResultMessage"
                        }
                    }
                }
            }
        }
    },
    "definitions": {
        "gndservice.HTTPResultMessage": {
            "type": "object",
            "properties": {
                "code": {
                    "type": "integer",
                    "example": 400
                },
                "message": {
                    "type": "string",
                    "example": "status bad request"
                }
            }
        },
        "marc21struct.Controlfield": {
            "type": "object",
            "properties": {
                "tag": {
                    "type": "string"
                },
                "text": {
                    "type": "string"
                }
            }
        },
        "marc21struct.Datafield": {
            "type": "object",
            "properties": {
                "ind1": {
                    "type": "string"
                },
                "ind2": {
                    "type": "string"
                },
                "subfield": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/marc21struct.Subfield"
                    }
                },
                "tag": {
                    "type": "string"
                }
            }
        },
        "marc21struct.Leader": {
            "type": "object",
            "properties": {
                "text": {
                    "type": "string"
                }
            }
        },
        "marc21struct.Record": {
            "type": "object",
            "properties": {
                "LDR": {
                    "$ref": "#/definitions/marc21struct.Leader"
                },
                "controlfield": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/marc21struct.Controlfield"
                    }
                },
                "datafield": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/marc21struct.Datafield"
                    }
                }
            }
        },
        "marc21struct.Subfield": {
            "type": "object",
            "properties": {
                "code": {
                    "type": "string"
                },
                "datafield": {
                    "$ref": "#/definitions/marc21struct.Datafield"
                },
                "text": {
                    "type": "string"
                }
            }
        }
    },
    "securityDefinitions": {
        "ApiKeyAuth": {
            "description": "Bearer Authentication with JWT",
            "type": "apiKey",
            "name": "Authorization",
            "in": "header"
        }
    }
}`

// SwaggerInfo holds exported Swagger Info so clients can modify it
var SwaggerInfo = &swag.Spec{
	Version:          "1.0",
	Host:             "",
	BasePath:         "",
	Schemes:          []string{},
	Title:            "GND API",
	Description:      "API for GND data. The API retrieves GND data from a local database that is updated once every hour.",
	InfoInstanceName: "swagger",
	SwaggerTemplate:  docTemplate,
	LeftDelim:        "{{",
	RightDelim:       "}}",
}

func init() {
	swag.Register(SwaggerInfo.InstanceName(), SwaggerInfo)
}
