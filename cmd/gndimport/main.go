package main

import (
	"crypto/tls"
	"gitlab.switch.ch/ub-unibas/gndservice/v2/pkg/gnd"
	ublogger "gitlab.switch.ch/ub-unibas/go-ublogger/v2"
	"go.ub.unibas.ch/cloud/certloader/v2/pkg/loader"
	"io"
	"log"
	"time"
)

func main() {
	config, err := LoadConfig()
	if err != nil {
		log.Fatalf("Cannot load configuration: %v", err)
		return
	}
	config.DisplayConfigLog()

	var loggerTLSConfig *tls.Config
	var loggerLoader io.Closer
	if config.Log.Stash.TLS != nil {
		loggerTLSConfig, loggerLoader, err = loader.CreateClientLoader(config.Log.Stash.TLS, nil)
		if err != nil {
			log.Fatalf("cannot create client loader: %v", err)
		}
		defer loggerLoader.Close()
	}

	logger, _logstash, _logfile, err := ublogger.CreateUbMultiLoggerTLS(config.Log.Level, config.Log.File,
		ublogger.SetDataset(config.Log.Stash.Dataset),
		ublogger.SetLogStash(config.Log.Stash.LogstashHost, config.Log.Stash.LogstashPort, config.Log.Stash.Namespace, config.Log.Stash.LogstashTraceLevel),
		ublogger.SetTLS(config.Log.Stash.TLS != nil),
		ublogger.SetTLSConfig(loggerTLSConfig),
	)
	if err != nil {
		log.Fatalf("cannot create logger: %v", err)
	}
	if _logstash != nil {
		defer _logstash.Close()
	}
	if _logfile != nil {
		defer _logfile.Close()
	}

	database, err := gnd.NewDatabase(
		config.DatabaseDirectory,
		config.DumpTargetDir,
		config.GndDumpBaseUrl,
		"",
		config.OaiSetAuthorityFilter,
		time.Duration(0),
		logger)
	if err != nil {
		logger.Fatal().Msgf("cannot create GND structure: %v", err)
	}
	defer database.Close()

	err = database.ImportData(config.DropExisting)
	if err != nil {
		logger.Fatal().Msgf("cannot import data: %v", err)
	}
	logger.Info().Msg("Data import finished")
}
