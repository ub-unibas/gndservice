package main

import (
	"emperror.dev/errors"
	"fmt"
	"github.com/BurntSushi/toml"
	"github.com/je4/utils/v2/pkg/stashconfig"
	"gitlab.switch.ch/ub-unibas/gndservice/v2/config"
	"gitlab.switch.ch/ub-unibas/gndservice/v2/pkg/shared"
	"io/fs"
	"log"
	"os"
	"path/filepath"
)

type ServiceConfig struct {
	DatabaseDirectory     string             `toml:"database_directory"`
	GndDumpBaseUrl        string             `toml:"gnd_dump_base_url"`
	DumpTargetDir         string             `toml:"dump_target_dir"`
	OaiSetAuthorityFilter string             `toml:"oai_set_authority_filter"`
	DropExisting          bool               `toml:"drop_existing"`
	Log                   stashconfig.Config `toml:"log"`
}

func LoadConfig() (*ServiceConfig, error) {
	var configFileSystem fs.FS
	var configFileName string
	if configFileLocation, ok := os.LookupEnv(shared.ConfigFileLocationEnvVariable); ok {
		fmt.Println("Loading config file from environment: ", configFileLocation)
		configFileSystem = os.DirFS(filepath.Dir(configFileLocation))
		configFileName = filepath.Base(configFileLocation)
	} else {
		fmt.Println("Loading embedded config file")
		configFileSystem = config.ConfigFS
		configFileName = "import.toml"
	}

	if _, directoryError := fs.Stat(configFileSystem, configFileName); directoryError != nil {
		path, err := os.Getwd()
		if err != nil {
			return nil, errors.Wrap(err, "cannot get current working directory")
		}
		configFileSystem = os.DirFS(path)
		configFileName = "import.toml"
	}

	data, readFileError := fs.ReadFile(configFileSystem, configFileName)
	if readFileError != nil {
		return nil, errors.Wrap(readFileError, "error reading config file")
	}

	configData := defaultConfig()

	_, tomlError := toml.Decode(string(data), configData)
	if tomlError != nil {
		return nil, errors.Wrap(tomlError, "toml error parsing config file")
	}

	return configData, nil
}

func defaultConfig() *ServiceConfig {
	return &ServiceConfig{
		DatabaseDirectory:     "data/database",
		GndDumpBaseUrl:        "https://data.dnb.de/GND",
		DumpTargetDir:         "data/download",
		OaiSetAuthorityFilter: "",
		DropExisting:          true,

		Log: stashconfig.Config{
			Level: "DEBUG",
			File:  "",
			Stash: stashconfig.StashConfig{},
		},
	}
}

func (c ServiceConfig) DisplayConfigLog() {
	log.Printf("Service Configuration:")
	log.Printf("Database Directory: %s", c.DatabaseDirectory)
	log.Printf("GND Dump Base URL: %s", c.GndDumpBaseUrl)
	log.Printf("Dump Target Directory: %s", c.DumpTargetDir)
	log.Printf("Oai Set Authority Filter: %s", c.OaiSetAuthorityFilter)
	log.Printf("Drop Existing Database: %t", c.DropExisting)
}
