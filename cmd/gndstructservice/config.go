package main

import (
	"emperror.dev/errors"
	"fmt"
	"github.com/BurntSushi/toml"
	"github.com/je4/utils/v2/pkg/stashconfig"
	"gitlab.switch.ch/ub-unibas/gndservice/v2/config"
	"gitlab.switch.ch/ub-unibas/gndservice/v2/pkg/shared"
	"io/fs"
	"log"
	"os"
	"path/filepath"
)

type ServiceConfig struct {
	GndServiceUrl      string `toml:"gnd_service_url"`
	MaxIdleConnections int    `toml:"max_idle_connections"`

	Server Server             `toml:"server"`
	Log    stashconfig.Config `toml:"log"`
}

type Server struct {
	Domain    string `toml:"domain"`
	Port      int    `toml:"port"`
	ProxyUri  string `toml:"proxy_uri"`
	EnableTls bool   `toml:"enable_tls"`
	CertFile  string `toml:"cert_file"`
	KeyFile   string `toml:"key_file"`
	CaFile    string `toml:"ca_file"`
}

func LoadConfig() (*ServiceConfig, error) {
	var configFileSystem fs.FS
	var configFileName string
	if configFileLocation, ok := os.LookupEnv(shared.ConfigFileLocationEnvVariable); ok {
		fmt.Println("Loading config file from environment: ", configFileLocation)
		configFileSystem = os.DirFS(filepath.Dir(configFileLocation))
		configFileName = filepath.Base(configFileLocation)
	} else {
		fmt.Println("Loading embedded config file")
		configFileSystem = config.ConfigFS
		configFileName = "import.toml"
	}

	if _, directoryError := fs.Stat(configFileSystem, configFileName); directoryError != nil {
		path, err := os.Getwd()
		if err != nil {
			return nil, errors.Wrap(err, "cannot get current working directory")
		}
		configFileSystem = os.DirFS(path)
		configFileName = "import.toml"
	}

	data, readFileError := fs.ReadFile(configFileSystem, configFileName)
	if readFileError != nil {
		return nil, errors.Wrap(readFileError, "error reading config file")
	}

	configData := defaultConfig()

	_, tomlError := toml.Decode(string(data), configData)
	if tomlError != nil {
		return nil, errors.Wrap(tomlError, "toml error parsing config file")
	}

	return configData, nil
}

func defaultConfig() *ServiceConfig {
	return &ServiceConfig{
		GndServiceUrl:      "http://localhost:9000",
		MaxIdleConnections: 3000,

		Server: Server{
			Domain:    "localhost",
			Port:      9001,
			ProxyUri:  "http://localhost:9001",
			EnableTls: false,
			CertFile:  "certs/tls.crt",
			KeyFile:   "certs/tls.key",
			CaFile:    "certs/ca.crt",
		},

		Log: stashconfig.Config{
			Level: "DEBUG",
			File:  "",
			Stash: stashconfig.StashConfig{},
		},
	}
}

func (c ServiceConfig) DisplayConfigLog() {
	log.Printf("Service Configuration:")
	log.Printf("GND Service Url: %s", c.GndServiceUrl)
	log.Printf("Max Idle Connections: %d", c.MaxIdleConnections)
	log.Printf("Server Domain: %s", c.Server.Domain)
	log.Printf("Server Port: %d", c.Server.Port)
	log.Printf("Server Proxy Uri: %s", c.Server.ProxyUri)
	log.Printf("Server EnableTls: %t", c.Server.EnableTls)
	log.Printf("Server Cert: %s", c.Server.CertFile)
	log.Printf("Server Key: %s", c.Server.KeyFile)
	log.Printf("Server Ca: %s", c.Server.CaFile)
}
