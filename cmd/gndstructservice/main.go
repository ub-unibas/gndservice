package main

import (
	"crypto/tls"
	"emperror.dev/errors"
	"gitlab.switch.ch/ub-unibas/gndservice/v2/pkg/gndstructservice"
	"gitlab.switch.ch/ub-unibas/gndservice/v2/pkg/shared"
	ublogger "gitlab.switch.ch/ub-unibas/go-ublogger/v2"
	"go.ub.unibas.ch/cloud/certloader/v2/pkg/loader"
	"golang.org/x/net/http2"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"sync"
	"syscall"
)

func main() {
	config, err := LoadConfig()
	if err != nil {
		log.Fatalf("Cannot load configuration: %v", err)
		return
	}
	config.DisplayConfigLog()

	var loggerTLSConfig *tls.Config
	var loggerLoader io.Closer
	if config.Log.Stash.TLS != nil {
		loggerTLSConfig, loggerLoader, err = loader.CreateClientLoader(config.Log.Stash.TLS, nil)
		if err != nil {
			log.Fatalf("cannot create client loader: %v", err)
		}
		defer loggerLoader.Close()
	}

	logger, _logstash, _logfile, err := ublogger.CreateUbMultiLoggerTLS(config.Log.Level, config.Log.File,
		ublogger.SetDataset(config.Log.Stash.Dataset),
		ublogger.SetLogStash(config.Log.Stash.LogstashHost, config.Log.Stash.LogstashPort, config.Log.Stash.Namespace, config.Log.Stash.LogstashTraceLevel),
		ublogger.SetTLS(config.Log.Stash.TLS != nil),
		ublogger.SetTLSConfig(loggerTLSConfig),
	)
	if err != nil {
		log.Fatalf("cannot create logger: %v", err)
	}
	if _logstash != nil {
		defer _logstash.Close()
	}
	if _logfile != nil {
		defer _logfile.Close()
	}

	http.DefaultTransport.(*http.Transport).MaxIdleConnsPerHost = config.MaxIdleConnections

	tlsConfig, err := shared.LoadCertificateConfig(
		config.Server.EnableTls,
		config.Server.CertFile,
		config.Server.KeyFile,
		config.Server.CaFile,
		logger,
	)
	if err != nil {
		log.Fatalf("Failed to load tls config: %v.", err)
		return
	}

	httpClient := &http.Client{Transport: &http2.Transport{
		DialTLSContext:             nil,
		TLSClientConfig:            tlsConfig,
		ConnPool:                   nil,
		DisableCompression:         false,
		AllowHTTP:                  false,
		MaxHeaderListSize:          0,
		MaxReadFrameSize:           0,
		MaxDecoderHeaderTableSize:  0,
		MaxEncoderHeaderTableSize:  0,
		StrictMaxConcurrentStreams: false,
		ReadIdleTimeout:            0,
		PingTimeout:                0,
		WriteByteTimeout:           0,
		CountError:                 nil,
	}}

	wg := &sync.WaitGroup{}
	gndServiceUrl, err := url.Parse(config.GndServiceUrl)
	if err != nil {
		logger.Fatal().Msgf("cannot parse url: %v", errors.Wrapf(err, "invalid external address '%s'", config.GndServiceUrl))
	}
	service, err := gndstructservice.Startup(
		config.Server.Domain,
		config.Server.Port,
		config.Server.ProxyUri,
		tlsConfig,
		gndServiceUrl,
		httpClient,
		wg,
		logger,
	)
	if err != nil {
		logger.Fatal().Msgf("could not start service: %v", err)
	}

	done := make(chan os.Signal, 1)
	signal.Notify(done, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)
	logger.Info().Msgf("press ctrl+c to stop server")
	s := <-done
	logger.Info().Msgf("Shutting down with signal: %v", s)

	service.GracefulStop()

	wg.Wait()
}
