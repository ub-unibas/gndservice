package main

import (
	"context"
	"crypto/tls"
	"gitlab.switch.ch/ub-unibas/gndservice/v2/pkg/gnd"
	"gitlab.switch.ch/ub-unibas/gndservice/v2/pkg/gndservice"
	"gitlab.switch.ch/ub-unibas/gndservice/v2/pkg/shared"
	ublogger "gitlab.switch.ch/ub-unibas/go-ublogger/v2"
	"go.ub.unibas.ch/cloud/certloader/v2/pkg/loader"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

func main() {
	http.DefaultTransport.(*http.Transport).MaxIdleConnsPerHost = 100
	config, err := LoadConfig()
	if err != nil {
		log.Fatalf("Cannot load configuration: %v", err)
		return
	}

	config.DisplayConfigLog()

	var loggerTLSConfig *tls.Config
	var loggerLoader io.Closer
	if config.Log.Stash.TLS != nil {
		loggerTLSConfig, loggerLoader, err = loader.CreateClientLoader(config.Log.Stash.TLS, nil)
		if err != nil {
			log.Fatalf("cannot create client loader: %v", err)
		}
		defer loggerLoader.Close()
	}

	logger, _logstash, _logfile, err := ublogger.CreateUbMultiLoggerTLS(config.Log.Level, config.Log.File,
		ublogger.SetDataset(config.Log.Stash.Dataset),
		ublogger.SetLogStash(config.Log.Stash.LogstashHost, config.Log.Stash.LogstashPort, config.Log.Stash.Namespace, config.Log.Stash.LogstashTraceLevel),
		ublogger.SetTLS(config.Log.Stash.TLS != nil),
		ublogger.SetTLSConfig(loggerTLSConfig),
	)
	if err != nil {
		log.Fatalf("cannot create logger: %v", err)
	}
	if _logstash != nil {
		defer _logstash.Close()
	}
	if _logfile != nil {
		defer _logfile.Close()
	}

	database, err := gnd.NewDatabase(config.DatabaseDirectory, "",
		"", config.OaiRepositoryUrl, config.OaiSetAuthorityFilter, time.Duration(config.GndSyncIntervalInHours)*time.Hour, logger)
	if err != nil {
		logger.Fatal().Msgf("cannot create GND structure: %v", err)
	}
	defer database.Close()

	wg := &sync.WaitGroup{}

	tlsConfig, err := shared.LoadCertificateConfig(
		config.Server.EnableTls,
		config.Server.CertFile,
		config.Server.KeyFile,
		config.Server.CaFile,
		logger,
	)
	if err != nil {
		logger.Fatal().Err(err).Msgf("Failed to load tls config: %v.", err)
	}

	service, err := gndservice.Startup(
		config.Server.Domain,
		config.Server.Port,
		config.Server.ProxyUri,
		tlsConfig,
		database,
		wg,
		logger,
	)
	if err != nil {
		logger.Fatal().Msgf("could not start service: %v", err)
	}

	var cancelSync context.CancelFunc
	if config.SyncIntervalInHours != 0 {
		cancelSync = database.Sync(time.Duration(config.SyncIntervalInHours)*time.Hour, wg)
	} else {
		if databaseError := database.Iterate(); databaseError != nil {
			logger.Error().Msgf("cannot sync: %v", databaseError)
		}
	}

	done := make(chan os.Signal, 1)
	signal.Notify(done, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)
	logger.Info().Msgf("Send kill signal to stop server.")
	s := <-done
	logger.Info().Msgf("Initiate shutdown: %s", s)

	service.GracefulStop()
	if cancelSync != nil {
		cancelSync()
	}

	wg.Wait()
}
