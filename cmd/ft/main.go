package main

import (
	"compress/gzip"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"github.com/je4/utils/v2/pkg/checksum"
	"io"
	"os"
	"sync"
)

func main() {
	fp, _ := os.Open("c:/temp/authorities-gnd-person_dnbmarc_20230613.mrc.xml.gz")
	defer fp.Close()
	pr, pw := io.Pipe()
	w, _ := checksum.NewChecksumWriter(
		[]checksum.DigestAlgorithm{checksum.DigestSHA256},
		pw,
	)
	wg := sync.WaitGroup{}
	wg.Add(1)
	go func() {
		defer wg.Done()
		if _, err := io.Copy(w, fp); err != nil {
			fmt.Printf("cannot copy body: %v\n")
		}
		if err := w.Close(); err != nil {
			fmt.Printf("cannot close checksum writer: %v\n", err)
		}
		if err := pw.Close(); err != nil {
			fmt.Printf("cannot close pipe: %v\n", err)
		}
	}()
	decomp, err := gzip.NewReader(pr)
	if err != nil {
		fmt.Printf("cannot create gzip reader: %v\n", err)
	}
	h := sha256.New()
	if _, err := io.Copy(h, decomp); err != nil {
		fmt.Printf("cannot copy content: %v\n", err)
	}
	wg.Wait()
	if err := decomp.Close(); err != nil {
		fmt.Printf("cannot close gzip reader: %v\n", err)
	}
	css, err := w.GetChecksums()
	if err != nil {
		fmt.Printf("cannot get checksums: %v\n", err)
	}
	fmt.Printf("Compressed: %s\n", css[checksum.DigestSHA256])
	fmt.Printf("deCompressed: %s\n", hex.EncodeToString(h.Sum(nil)))
}
