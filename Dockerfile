FROM golang:1.23.3 AS builder

ENV GO111MODULE=on
ENV GOPRIVATE=gitlab.switch.ch/ub-unibas/*
ENV CGO_ENABLED=0
ENV GOOS=linux
ENV GOARCH=amd64

WORKDIR /source
COPY . /source
RUN go mod download
WORKDIR /source/cmd/gndimport
RUN go build -o /app/gndimport
WORKDIR /source/cmd/gndservice
RUN go build -o /app/gndservice
WORKDIR /source/cmd/gndstructservice
RUN go build -o /app/gndstructservice

FROM alpine:latest
WORKDIR /
COPY --from=builder /app /app
EXPOSE 8080

ENTRYPOINT ["/app/gndservice"]