package config

import (
	"embed"
)

//go:embed api.toml
//go:embed struct.toml
//go:embed import.toml
var ConfigFS embed.FS
