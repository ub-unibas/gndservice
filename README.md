# GND Service
This service provides an API to map MARC21 records with GND IDs to their corresponding GND records. There are three
components to this service:

1. **gnd service**: This service manages the database and returns gnd metadata in a JSON format 
  for a given GND ID.
2. **gnd struct service**: This service sits in front of the **gnd service** and accepts MARC21 documents in JSON format
   and returns the enriched JSON document with GND metadata.
3. **gnd import**. A service to download the GND record files from the DNB and import them into the database. 
   This service is only used for the initial database creation. After that the gndservice will update the 
   database with the latest records through the OAI-PMH API.

## GND Struct Service
The gndservice provides APIs to map access points from [MARC21](https://www.loc.gov/marc/) records and enrich them with data from the [GND](https://www.dnb.de/DE/Professionell/Standardisierung/GND/gnd_node.html). GND data is harvested in MARC21 through the OAI-PMH interface provided by the [DNB](https://www.dnb.de/DE/Professionell/Metadatendienste/Datenbezug/OAI/oai_node.html). The service is tailored to data from [SLSP](https://slsp.ch/de) but can be used for other data sources providing MARC data.

The following APIs are provided:
* **person**, processing of fields 100/700 with access points for persons: /api/v1/person
* **subject.person**, processing of fields 600 with access points for persons: /api/v1/subject.person
* **family**, processing of fields 100/700, first indicator 3, with access points for families: /api/v1/family
* **subject.family**, processing of fields 600, first indicator 3, with access points for families: /api/v1/subject.family
* **corporate**, processing of fields 110/710 with access points for corporates: /api/v1/corporate
* **subject.corporate**, processing of fields 610 with access points for corporates: /api/v1/subject.corporate
* **conference**, processing of fields 111/711 with access points for conferences: /api/v1/conference
* **subject.conference**, processing of fields 611 with access points for conferences: /api/v1/subject.conference
* **geo**, processing of fields 751: /api/v1/geo
* **subject.geo**, processing of fields 651: /api/v1/subject.geo
* **title**, processing of fields 1xx/7xx with access points for titles: /api/v1/title
* **subject.title**, processing of fields 6xx with access points for titles: /api/v1/subject.title
* **topic**, processing of fields 650: /api/v1/topic

All APIs for fields 1xx/7xx also process linked access points from fields 880.

ISBD interpunction is removed from Community Zone data.

A detailed description of the mapping and enrichment including examples is provided [here](https://gitlab.switch.ch/ub-unibas/gndservice/-/blob/main/documentation).

## Usage


## Test

```bash
docker build -t test:latest .

docker run --env-file .env -v ./certs:/certs test:latest

docker run --env-file .env -v ./certs:/certs --entrypoint /app/gndstructservice test:latest

ssh -L 5046:ub-log.ub.unibas.ch:5046 root@ub-log.ub.unibas.ch
```

## Support
Jürgen Enge, Silvia Witzig

## Authors and acknowledgment
Jürgen Enge, Silvia Witzig

## License


## Project status
Development
